# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Changed

- Allow zoom room names to contain symbols
- Updated syntax for specifying the role directory
- Changed ordering of team groups to show users with no team specified last in the participants list.
- Changed run configuration for nginx with an ssl certificate (to support running the server as https)
- use of FastAPI lifespan events for start up and shutdown

### Fixed

- Users specified in the google sheets settings with an empty team no longer show up with an empty team title.
  Instead they appear in the 'no team set' group.
- Fixed handling of Miro API tokens for auth in the request header.
