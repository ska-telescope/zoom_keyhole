# Zoom Keyhole

[![Documentation Status](https://readthedocs.org/projects/zoom-keyhole/badge/?version=latest)](https://zoom-keyhole.readthedocs.io/en/latest/?badge=latest)

A simple web application that displays Zoom rooms' participants on Miro boards.

## Quick-start

### Setting up Miro board widgets

Within a Miro board, a text widget needs to be created with the configured Zoom meeting name and the text: "Meeting ID:". The meeting ID itself will be added as a clickable link when the application runs.

### Google Sheets configuration

The current Google Sheets config can be found [here](https://docs.google.com/spreadsheets/d/1XzmbI33G9FDU_JCvtZLCz9Ibd5eyJmZZPZ_YlxRo8G4/edit?usp=sharing). It contains the following tabs:

- Users
- Rooms
- Boards
- Role symbols

#### Users

A new user is added with their name the same as that displayed on Zoom. A Unique Partial ID can be added if their Zoom display name is an email or another ID. Their role is given as specified in the 'Role symbols' tab. Their team, ART, and a custom emoji can also be added.

#### Rooms

A room must have the same name as that given in the Miro text widget. The meeting ID must also be supplied but the URL is optional. A UTC offset can be added which will be displayed with the local time in the widget. A room can also be enabled/ disabled using the enabled checkbox.

#### Boards

A board is added with its name and ID, which can be found in the URL. A board can also be enabled/ disabled using the checkbox.

#### Role symbols

The role symbols contains the various roles and their associated symbols, which are displayed next to the users in the widget.

### Required environment variables

Zoom Keyhole requires that the following environment variables are set:

- `ZOOM_KEYHOLE_MIRO_API_TOKEN`: Miro API token to use. A semi-colon separated list
  can be used to specify more than one token. If multiple tokens are provided, Miro API
  calls are shared between them. **Note: For kubernetes deployments this is : seperated list** 
- `ZOOM_KEYHOLE_ZOOM_CLIENT_ID`: Client ID for the Zoom OAuth app. Needed for polling
  the Zoom API for room participants. Used when getting initial room list and
  periodically for updating participants.
- `ZOOM_KEYHOLE_ZOOM_CLIENT_SECRET`: Client secret for the Zoom OAuth app. Used in
  combination with the Zoom Client ID and Account ID.
- `ZOOM_KEYHOLE_ZOOM_ACCOUNT_ID`: Zoom Account ID. Used in combination with the Zoom
  Client ID and Client secret.
- `ZOOM_KEYHOLE_WEBHOOK_SECRET_TOKEN`: Zoom Webhook Secret Token used in verifying the authencity of webhook payloads.
- `ZOOM_KEYHOLE_GSHEET_KEY`: API Key for google sheets. Used to load google sheets configuration.
- `ZOOM_KEYHOLE_GSHEET_ID`: Google sheet ID where the configuration is found.


## Deployment with Kubernetes

The included chart requires the all of the sensitive environment variable values to be passed in at runtime.  This can be done by providing a `values.yaml` file:

```shell
helm install zoom ./charts/ska-zoom-keyhole --values values.yaml
```

An example (redacted) file is:

```yaml
---
env:
  ZOOM_KEYHOLE_MIRO_API_TOKEN: "<a token>"
  ZOOM_KEYHOLE_ZOOM_CLIENT_ID: <client id>
  ZOOM_KEYHOLE_ZOOM_CLIENT_SECRET: <secret>
  ZOOM_KEYHOLE_ZOOM_ACCOUNT_ID: <account id>
  ZOOM_KEYHOLE_WEBHOOK_SECRET_TOKEN: <secret>
  ZOOM_KEYHOLE_GSHEET_KEY: <key>
  ZOOM_KEYHOLE_GSHEET_ID: <id>
```

### Ingress URI

The default ingress URI is `/ska/zoom-keyhole` (values .ingress.*), so if this were deployed in STFC TechOps the the URL would be `https://k8s.stfc.skao.int/ska/zoom-keyhole` (Note: in TechOps a rule is required in the reverse proxy to enable unauthenticated access to this resource).

### Vault Secret Integration

The Vault secrets are maintained at: `https://vault.skao.int/ui/vault/secrets/stfc/show/ska-zoom-keyhole` .

These cover the key/values:

- ZOOM_KEYHOLE_MIRO_API_TOKEN
- ZOOM_KEYHOLE_ZOOM_CLIENT_ID
- ZOOM_KEYHOLE_ZOOM_CLIENT_SECRET
- ZOOM_KEYHOLE_ZOOM_ACCOUNT_ID
- ZOOM_KEYHOLE_WEBHOOK_SECRET_TOKEN
- ZOOM_KEYHOLE_GSHEET_KEY
- ZOOM_KEYHOLE_GSHEET_ID

Identical to the `values.yaml` file settings.
