apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "ska-zoom-keyhole.fullname" . }}
  labels:
    {{- include "ska-zoom-keyhole.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "ska-zoom-keyhole.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
{{- if .Values.vault.useVault }}
{{- if not .Values.podAnnotations }}
      annotations:
{{- end }}
# see: https://github.com/hashicorp/consul-template/blob/main/docs/templating-language.md
        vault.hashicorp.com/agent-inject: "true"
        vault.hashicorp.com/agent-inject-status: "update"
        vault.hashicorp.com/agent-inject-secret-config.yaml: "{{ .Values.vault.secretPath }}/ska-zoom-keyhole"
        vault.hashicorp.com/agent-inject-template-config.yaml: |
          {{`{{- with secret `}}"{{ .Values.vault.secretPath }}/ska-zoom-keyhole"{{` -}}`}}
          {{`{{ .Data.data.zoomKeyholeConfig }}`}}
          {{`{{- end }}`}}
        vault.hashicorp.com/agent-inject-secret-config: "{{ .Values.vault.secretPath }}/ska-zoom-keyhole"
        vault.hashicorp.com/agent-inject-template-config: |
          {{`{{- with secret `}}"{{ .Values.vault.secretPath }}/ska-zoom-keyhole"{{` -}}`}}
          {{`{{- range $k, $v := .Data.data }}`}}
          {{`{{- if ne $k "zoomKeyholeConfig" }}`}}
          {{`export {{ $k }}={{ $v }}`}}
          {{`{{- end }}`}}
          {{`{{- end }}`}}
          {{`{{- end }}`}}
        vault.hashicorp.com/role: "{{ .Values.vault.role }}"
{{- end }}
      labels:
        {{- include "ska-zoom-keyhole.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "ska-zoom-keyhole.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      volumes:
      - name: configuration
        configMap:
          name: {{ include "ska-zoom-keyhole.fullname" . }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          volumeMounts:
            - name: configuration
              mountPath: {{ .Values.env.ZOOM_KEYHOLE_CONFIG_FILE }}
              subPath: zoom-keyhole.yaml
              readOnly: true
          env:
          # Zoom
          - name: ZOOM_KEYHOLE_ZOOM_CLIENT_ID
            value: {{ .Values.env.ZOOM_KEYHOLE_ZOOM_CLIENT_ID }}
          - name: ZOOM_KEYHOLE_ZOOM_CLIENT_SECRET
            value: {{ .Values.env.ZOOM_KEYHOLE_ZOOM_CLIENT_SECRET }}
          - name: ZOOM_KEYHOLE_ZOOM_ACCOUNT_ID
            value: {{ .Values.env.ZOOM_KEYHOLE_ZOOM_ACCOUNT_ID }}
          - name: ZOOM_KEYHOLE_WEBHOOK_SECRET_TOKEN
            value: {{ .Values.env.ZOOM_KEYHOLE_WEBHOOK_SECRET_TOKEN }}
          - name: ZOOM_ROOM_SCAN_PERIOD
            value: {{ .Values.env.ZOOM_ROOM_SCAN_PERIOD | quote }}
          - name: ZOOM_KEYHOLE_CONFIG_FILE
            value: {{ .Values.vault.useVault | ternary "/vault/secrets/config.yaml" .Values.env.ZOOM_KEYHOLE_CONFIG_FILE }}
          # Miro
          - name: ZOOM_KEYHOLE_MIRO_API_TOKEN
            value: {{ .Values.env.ZOOM_KEYHOLE_MIRO_API_TOKEN | replace ":" ";"}}
          - name: MIRO_BOARD_SCAN_PERIOD
            value: {{ .Values.env.MIRO_BOARD_SCAN_PERIOD | quote }}
          - name: MIRO_BOARD_UPDATE_PERIOD
            value: {{ .Values.env.MIRO_BOARD_UPDATE_PERIOD | quote }}
          # Google
          - name: ZOOM_KEYHOLE_GSHEET_KEY
            value: {{ .Values.env.ZOOM_KEYHOLE_GSHEET_KEY }}
          - name: ZOOM_KEYHOLE_GSHEET_ID
            value: {{ .Values.env.ZOOM_KEYHOLE_GSHEET_ID }}
          - name: GSHEET_USERS_SCAN_PERIOD
            value: {{ .Values.env.GSHEET_USERS_SCAN_PERIOD | quote }}
          - name: GSHEET_BOARDS_SCAN_PERIOD
            value: {{ .Values.env.GSHEET_BOARDS_SCAN_PERIOD | quote }}
          - name: GSHEET_ROOMS_SCAN_PERIOD
            value: {{ .Values.env.GSHEET_ROOMS_SCAN_PERIOD | quote }}
          - name: GSHEET_ROLES_SCAN_PERIOD
            value: {{ .Values.env.GSHEET_ROLES_SCAN_PERIOD | quote }}
          # Misc
          - name: ROOT_PATH
            value: {{ .Values.env.ROOT_PATH }}
          - name: KEEP_ALIVE
            value: "50"
          - name: LOG_LEVEL
            value: "debug"
{{ if .Values.vault.useVault }}
          command: ["bash"]
          args:
          - "-c"
          - ". /vault/secrets/config || true && uvicorn src.zoom_keyhole.main:APP --loop asyncio --host 0.0.0.0 --forwarded-allow-ips='*' --root-path $(ROOT_PATH)"
{{ else }}
          command: ["uvicorn"]
          args:
          - "src.zoom_keyhole.main:APP"
          - "--loop"
          - "asyncio"
          - "--host"
          - "0.0.0.0"
          - "--forwarded-allow-ips='*'"
          - "--root-path"
          - "$(ROOT_PATH)"
{{ end }}
          ports:
            - name: http
              containerPort: 8000
              protocol: TCP
          readinessProbe:
            tcpSocket:
              port: 8000
            initialDelaySeconds: 5
            periodSeconds: 10
          livenessProbe:
            tcpSocket:
              port: 8000
            initialDelaySeconds: 15
            periodSeconds: 20
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
