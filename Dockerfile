#
# This Dockerfile creates a Docker image containing the Zoom Keyhole App
# python project.
#
ARG BASE_IMAGE="tiangolo/uvicorn-gunicorn-fastapi:python3.10"
FROM $BASE_IMAGE
ARG DEBIAN_FRONTEND=noninteractive
ARG CAR_OCI_REGISTRY_HOST

LABEL \
    author="Piers Harding <piers.harding@skao.int>" \
    description="This image contains the Zoom Keyhole App" \
    license="BSD-3-Clause" \
    registry="${CAR_OCI_REGISTRY_HOST}/ska-zoom-keyhole" \
    int.skao.team="Systems Team" \
    int.skao.version="1.0.0"

USER root

ARG ROOT_PATH_ARG="ska/zoom-keyhole"
ENV ROOT_PATH=$ROOT_PATH_ARG \
    POETRY_HOME=/opt/poetry \
    POETRY_VERSION=1.5.1 \
    PYTHONPATH=/app/src:/usr/local/lib/python3.10/site-packages \
    PATH="/opt/poetry/bin:$PATH"

WORKDIR /app

# Install poetry and dependencies in a single layer
RUN mkdir -p "$POETRY_HOME" && \
    curl -sSL https://raw.githubusercontent.com/python-poetry/install.python-poetry.org/main/install-poetry.py --output "$POETRY_HOME"/install-poetry.py && \
    cd "$POETRY_HOME" && \
    POETRY_VERSION="${POETRY_VERSION}" python3 install-poetry.py --yes && \
    poetry config virtualenvs.create false && \
    pip install --no-cache-dir --upgrade pip

# Install dependencies
COPY pyproject.toml poetry.lock* ./
RUN poetry export --format requirements.txt --output requirements.txt --without-hashes && \
    pip install --no-cache-dir -r requirements.txt && \
    rm -rf requirements.txt ~/.cache/pip

# Copy application code
COPY ./src /app/src

# Launch
ENTRYPOINT ["uvicorn"]
CMD ["src.zoom_keyhole.main:APP", "--host", "0.0.0.0", "--loop", "asyncio", "--forwarded-allow-ips='*'", "--root-path", "${ROOT_PATH}"]
