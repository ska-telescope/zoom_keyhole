PROJECT = ska-zoom-keyhole

# include core make support
include .make/base.mk

# Include Python support
# PYTHON_LINE_LENGTH = 79
include .make/python.mk

#  OCI image support
include .make/oci.mk

#  Helm support
include .make/helm.mk

#  K8s support
include .make/k8s.mk

#######################################################
# Start Zoom Keyhole
#######################################################

# Load .env file if found.
ifneq ("$(wildcard .env)","")
include .env
$(eval export $(shell sed 's/=.*//' .env))
endif

# Use repo image and Vault for STFC deployments
ifeq ($(CI_ENVIRONMENT_NAME),integration-ska-zoom-keyhole)
K8S_CHART_PARAMS += --set image.repository=registry.gitlab.com/ska-telescope/ska-zoom-keyhole/ska-zoom-keyhole \
	--set image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	--set env.ZOOM_KEYHOLE_ZOOM_CLIENT_ID=$(ZOOM_KEYHOLE_ZOOM_CLIENT_ID) \
	--set env.ZOOM_KEYHOLE_ZOOM_CLIENT_SECRET=$(ZOOM_KEYHOLE_ZOOM_CLIENT_SECRET) \
	--set env.ZOOM_KEYHOLE_ZOOM_ACCOUNT_ID=$(ZOOM_KEYHOLE_ZOOM_ACCOUNT_ID) \
	--set env.ZOOM_KEYHOLE_WEBHOOK_SECRET_TOKEN=$(ZOOM_KEYHOLE_WEBHOOK_SECRET_TOKEN) \
	--set env.ZOOM_ROOM_SCAN_PERIOD=$(ZOOM_ROOM_SCAN_PERIOD) \
	--set env.ZOOM_KEYHOLE_GSHEET_KEY=$(ZOOM_KEYHOLE_GSHEET_KEY) \
	--set env.ZOOM_KEYHOLE_GSHEET_ID=$(ZOOM_KEYHOLE_GSHEET_ID) \
	--set env.GSHEET_USERS_SCAN_PERIOD=$(GSHEET_USERS_SCAN_PERIOD) \
	--set env.GSHEET_BOARDS_SCAN_PERIOD=$(GSHEET_BOARDS_SCAN_PERIOD) \
	--set env.GSHEET_ROOMS_SCAN_PERIOD=$(GSHEET_ROOMS_SCAN_PERIOD) \
	--set env.GSHEET_ROLES_SCAN_PERIOD=$(GSHEET_ROLES_SCAN_PERIOD) \
	--set env.ZOOM_KEYHOLE_MIRO_API_TOKEN=$(ZOOM_KEYHOLE_MIRO_API_TOKEN) \
	--set env.MIRO_BOARD_SCAN_PERIOD=$(MIRO_BOARD_SCAN_PERIOD) \
	--set env.MIRO_BOARD_UPDATE_PERIOD=$(MIRO_BOARD_UPDATE_PERIOD) \
	--set env.ROOT_PATH="/ska/zoom-keyhole" \
	--set env.ZOOM_KEYHOLE_FILE_LOGGER=0
endif

run-rpi:  ## Run Zoom-keyhole on the Raspberry Pi
	uvicorn src.zoom_keyhole.main:APP --uds /tmp/uvicorn.sock --loop asyncio --forwarded-allow-ips='*'
.PHONY: run

run:  ## Run Zoom-keyhole on localhost
	uvicorn src.zoom_keyhole.main:APP --host 0.0.0.0 --port 8000 --loop asyncio --reload
.PHONY: run
