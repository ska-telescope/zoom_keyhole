# Zoom Keyhole Server Data

Zoom keyhole internally stores data in a global dictionary called CONFIG.

This has the following fields:

* `zoom_rooms`: Zoom room configuration. **Google sheet settings**
* `zoom_data`: Zoom room data. Miro widget configuration data + widget text.
* `directories`: Zoom directory data. Miro widget configuration data + widget text.
* `user_roles`: Mapping of roles to role icons. **Google sheet settings**
* `users`: Users configuration data. **Google sheet settings**
