"""Configuration file for the Sphinx documentation builder."""

# pylint: disable=invalid-name
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#

import os
import sys


sys.path.insert(0, os.path.abspath(".."))

package_version = {}
version_file = "../../src/zoom_keyhole/__version__.py"
# pylint: disable=exec-used
with open(version_file, "r", encoding="utf-8") as file:
    exec(file.read(), package_version)

# -- Project information -----------------------------------------------------

project = "Zoom Keyhole"
copyright = "2021, SKA Observatory"
author = "Fred Dulwich, Ben Mort, Ferdl Graser, Ugur Yilmaz"

# The full version, including alpha/beta/rc tags
release = "1.0.0"
version = "1.0.0"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ["sphinx_rtd_theme", "myst_parser", "sphinx_gitstamp"]

# Add any paths that contain templates here, relative to this directory.

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

source_suffix = {".rst": "restructuredtext", ".md": "markdown"}

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "ska_ser_sphinx_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
