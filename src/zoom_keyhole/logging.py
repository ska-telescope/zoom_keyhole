"""Module with common variables used for Zoom Keyhole."""

import logging
import os
import sys

LOG = logging.getLogger("zoom_keyhole")


def init_logger(level: str = "INFO"):
    """Initialise the logger.

    Args:
        level (str): log level
    """
    logger = logging.getLogger("zoom_keyhole")
    handler = logging.StreamHandler(sys.stdout)
    format_str = (
        "%(asctime)s : %(levelname)-7s : "
        + "%(message)-100s [%(filename)s L%(lineno)d]"
    )
    formatter = logging.Formatter(format_str, "%H:%M:%S")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    if bool(int(os.getenv("ZOOM_KEYHOLE_FILE_LOGGER", "0"))):
        print("Logging to file!")
        file_handler = logging.handlers.TimedRotatingFileHandler(
            "zkh.log", when="M", interval=30, backupCount=50, utc=True
        )
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
    logger.setLevel(level)


RED_CIRCLE = "\U0001F534"  # 🔴
RAISED_HAND = "\U0000270B"  # ✋
CHECK_MARK = "\U00002705"  # ✅
CLOCK1 = "\U0001F551"  # 🕑
