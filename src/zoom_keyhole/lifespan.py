"""Module with FastAPI server lifespan logic."""

import threading
import time
from contextlib import asynccontextmanager
from typing import Callable, Union

from fastapi import FastAPI

from .config import (
    CONFIG,
    GSHEET_BOARDS_SCAN_PERIOD,
    GSHEET_ROLES_SCAN_PERIOD,
    GSHEET_ROOMS_SCAN_PERIOD,
    GSHEET_USERS_SCAN_PERIOD,
    MIRO_BOARD_SCAN_PERIOD,
    MIRO_BOARD_UPDATE_PERIOD,
    initialise_config,
    load_gsheet_miro_boards,
    load_gsheet_rooms,
    load_gsheet_user_roles,
    load_gsheet_users,
)
from .logging import LOG, init_logger
from .miro_utils import scan_boards
from .periodic import Periodic
from .update_miro import (
    update_directory_widgets,
    update_miro,
    update_zoom_room_widgets,
)
from .zoom_utils import CONDITION_VAR, scan_rooms

BACKGROUND_TASKS = []
THREAD_POLL_ZOOM = threading.Thread(target=scan_rooms)
UNKNOWN_TEXT = "(Unknown)"


# pylint: disable=unused-argument
@asynccontextmanager
async def lifespan(app: FastAPI):
    """Lifespan for the zoom keyhole server."""

    # Initialise logger
    init_logger("DEBUG")

    LOG.info("Starting Zoom keyhole App")
    await initialise_startup_tasks()
    yield
    await perform_shutdown_tasks()


async def initialise_startup_tasks() -> None:
    """Initialise startup tasks for the server

    Includes Logging, configuration initialisation,
    and starting background tasks.
    """
    LOG.info("Initialising startup tasks ...")

    LOG.info("Initialising configuration...")
    initialise_config()

    LOG.info("Scheduling periodic tasks...")
    add_periodic_task(load_gsheet_miro_boards, GSHEET_BOARDS_SCAN_PERIOD)
    add_periodic_task(load_gsheet_rooms, GSHEET_ROOMS_SCAN_PERIOD)
    add_periodic_task(load_gsheet_users, GSHEET_USERS_SCAN_PERIOD)
    add_periodic_task(load_gsheet_user_roles, GSHEET_ROLES_SCAN_PERIOD)
    add_periodic_task(scan_boards, MIRO_BOARD_SCAN_PERIOD)
    add_periodic_task(update_miro, MIRO_BOARD_UPDATE_PERIOD)

    for task in BACKGROUND_TASKS:
        await task.start()

    LOG.info("Starting separate thread for polling zoom rooms...")
    THREAD_POLL_ZOOM.start()

    LOG.info("Startup tasks completed successfully.")


async def perform_shutdown_tasks() -> None:
    """
    Perform shutdown tasks for the server.

    Includes stopping tasks, cleaning up resources, and shutting down threads.
    """
    LOG.info("Performing shutdown tasks...")

    try:
        # Signal shutdown in configuration
        CONFIG["shutdown"] = True

        # Stop background tasks
        LOG.info("Stopping background tasks...")
        for task in BACKGROUND_TASKS:
            await task.stop()

        # Reset and clean up resources
        LOG.info("Resetting widgets and resources...")
        reset_timer = time.time()
        for room in CONFIG["zoom_data"]:
            CONFIG["zoom_data"][room].get("participants", []).clear()
            CONFIG["zoom_participant_location"].clear()
            if room in CONFIG["zoom_rooms"]:
                CONFIG["zoom_rooms"][room]["utc_offset"] = ""
            update_zoom_room_widgets(room, UNKNOWN_TEXT, True)
        update_directory_widgets(True)
        LOG.info(
            "Resource cleanup took %.1f seconds.", time.time() - reset_timer
        )

        # Stop the background thread for Zoom room scanning
        LOG.info("Stopping Zoom room scanning thread...")
        with CONDITION_VAR:
            CONDITION_VAR.notify_all()
        THREAD_POLL_ZOOM.join(timeout=10)

        LOG.info("Shutdown tasks completed successfully.")

    except KeyError as error:
        LOG.error(
            "Failed to access configuration key during shutdown: %s",
            error,
            exc_info=True,
        )
    except RuntimeError as error:
        LOG.error(
            "Runtime error during shutdown tasks: %s", error, exc_info=True
        )
    except TimeoutError:
        LOG.error(
            "Timeout while waiting for Zoom polling thread to stop",
            exc_info=True,
        )


def add_periodic_task(func: Callable, period: Union[int, float]) -> None:
    """
    Schedule a periodic task.

    Args:
        func (Callable): The task function to run.
        period (Union[int, float]): The interval between runs in seconds.

    Raises:
        ValueError: If the period is non-positive.
    """
    if period <= 0:
        raise ValueError("Period must be greater than zero.")

    name = getattr(func, "__name__", None)
    if name is None:
        raise ValueError(
            "Callable must have a __name__ attribute to infer its name"
        )

    LOG.info("\u23F0 Scheduling '%s' every %.0f seconds", name, period)
    BACKGROUND_TASKS.append(Periodic(func, period, name))
