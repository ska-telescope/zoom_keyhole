"""Methods to deal with configuration, including from Google Sheets."""

import json
import os
import threading
import time

import httplib2
from apiclient import discovery, errors

from .logging import LOG

# Update periods
GSHEET_USERS_SCAN_PERIOD = int(os.getenv("GSHEET_USERS_SCAN_PERIOD", "15"))
GSHEET_BOARDS_SCAN_PERIOD = int(os.getenv("GSHEET_BOARDS_SCAN_PERIOD", "120"))
GSHEET_ROOMS_SCAN_PERIOD = int(os.getenv("GSHEET_ROOMS_SCAN_PERIOD", "60"))
GSHEET_ROLES_SCAN_PERIOD = int(os.getenv("GSHEET_ROLES_SCAN_PERIOD", "120"))
ZOOM_ROOM_SCAN_PERIOD = int(os.getenv("ZOOM_ROOM_SCAN_PERIOD", "90"))
MIRO_BOARD_SCAN_PERIOD = int(os.getenv("MIRO_BOARD_SCAN_PERIOD", "60"))
MIRO_BOARD_UPDATE_PERIOD = int(os.getenv("MIRO_BOARD_UPDATE_PERIOD", "2"))

GSHEET_ID_WARNING = "Google Sheet ID (ZOOM_KEYHOLE_GSHEET_ID) not set!"
HEADER_PARTICIPANT_LIST = "Currently in the room"

PLANTS = [
    "\U0001f337",
    "\U0001f331",
    "\U0001f332",
    "\U0001f333",
    "\U0001f33b",
    "\U0001f33c",
    "\U0001f33d",
    "\U0001f33f",
    "\U0001f340",
    "\U0001f341",
]
FRUIT = [
    "\U0001f347",
    "\U0001f349",
    "\U0001f34a",
    "\U0001f34d",
    "\U0001f34e",
    "\U0001f34f",
    "\U0001f352",
    "\U0001f353",
    "\U0001f95d",
    "\U0001f965",
]
ANIMALS = [
    "\U0001f407",
    "\U0001f408",
    "\U0001f415",
    "\U0001f420",
    "\U0001f42c",
    "\U0001f42f",
    "\U0001f981",
    "\U0001f989",
    "\U0001f98a",
    "\U0001f98b",
]
SYMBOLS = PLANTS + FRUIT + ANIMALS

ALLOWED_ARTS = {"DP", "OMC", "Services", "Low", "Mid", "None", "All"}


# In-memory Zoom Keyhole configuration
CONFIG = {}


def initialise_config():
    """Initialise the configuration data."""
    # Google sheet settings
    CONFIG["users"] = []
    CONFIG["user_roles"] = {
        "symbol_position": "left",
        "show_bullet_point": True,
    }
    CONFIG["zoom_rooms"] = {}

    # Miro data
    CONFIG["zoom_data"] = {}
    CONFIG["zoom_participant_location"] = {}
    CONFIG["directories"] = []
    CONFIG["_mutex"] = threading.Lock()
    CONFIG["header_participant_list"] = HEADER_PARTICIPANT_LIST
    CONFIG["allowed_arts"] = ALLOWED_ARTS
    CONFIG["rooms_scanned"] = False


def load_gsheet_range(sheet_id: str, range_name: str) -> dict:
    """Load data from a range of cells in a Google Sheet.

    Args:
        sheet_id (str): Google Sheet ID.
        range_name (str): Sheet name and range, e.g. "'Users'!A2:F500".

    Returns:
        response (dict): Result of request, or an empty dict if there is an
            error.
    """
    # Skip if no spreadsheet ID specified.
    if not sheet_id:
        return {}

    # Get the API token to use.
    discovery_url = "https://sheets.googleapis.com/$discovery/rest?version=v4"
    service = discovery.build(
        "sheets",
        "v4",
        http=httplib2.Http(),
        discoveryServiceUrl=discovery_url,
        developerKey=os.getenv("ZOOM_KEYHOLE_GSHEET_KEY"),
    )

    # Call the Sheets API.
    # pylint: disable=no-member
    sheet = service.spreadsheets()
    request = sheet.values().get(spreadsheetId=sheet_id, range=range_name)

    # Try to execute the request.
    try:
        response = request.execute()
    except errors.HttpError as http_error:
        try:
            error_str = json.loads(http_error.content.decode("utf-8"))
        except json.JSONDecodeError:
            error_str = "(no error string)"
        LOG.error("Error from Google Sheets: %s", error_str)
        response = {}

    return response


def load_gsheet_miro_boards():
    """Load Miro board data from Google Sheets into CONFIG."""
    sheet_id = os.getenv("ZOOM_KEYHOLE_GSHEET_ID")
    if not sheet_id:
        LOG.warning(GSHEET_ID_WARNING)
        return
    timer = time.time()
    result = load_gsheet_range(sheet_id, "'Boards'!A2:C20")
    if not result:
        return
    CONFIG["miro_boards"] = []
    for row in result.get("values", []):
        for _ in range(len(row), 3):
            row.append("")
        name = row[0]
        if not name:
            continue

        CONFIG["miro_boards"].append(
            {"name": name, "id": row[1], "enabled": row[2] != "FALSE"}
        )
    LOG.info(
        "\U0001f4c3 Loaded %d Miro boards from Google Sheets in %.1f s",
        len(CONFIG["miro_boards"]),
        time.time() - timer,
    )


def load_gsheet_users():
    """Load user config data from a Google Sheet into CONFIG."""
    sheet_id = os.getenv("ZOOM_KEYHOLE_GSHEET_ID")
    if not sheet_id:
        LOG.warning(GSHEET_ID_WARNING)
        return
    timer = time.time()
    result = load_gsheet_range(sheet_id, "'Users'!A2:F700")
    if not result:
        return
    with CONFIG["_mutex"]:
        CONFIG["users"].clear()
    for row in result.get("values", []):
        for _ in range(len(row), 6):
            row.append("")
        name = row[0]
        if not name:
            continue
        ids = [id.strip() for id in row[1].split(",")]
        valid_ids = ids and all(len(id) > 1 for id in ids)

        with CONFIG["_mutex"]:
            roles = [role.lstrip().rstrip() for role in row[2].split(",")]
            teams = [team.lstrip().rstrip() for team in row[3].split(",")]
            CONFIG["users"].append(
                {
                    "name": name,
                    "ids": ids if valid_ids else [name],
                    "roles": roles,
                    "team": teams,
                    "ART": row[4],
                    "emoji": row[5].strip(),
                }
            )
    LOG.info(
        "\U0001f4c3 Loaded %d users from Google Sheet in %.1f s",
        len(CONFIG["users"]),
        time.time() - timer,
    )


def load_gsheet_rooms():
    """Load room config data from a Google Sheet into CONFIG."""
    sheet_id = os.getenv("ZOOM_KEYHOLE_GSHEET_ID")
    if not sheet_id:
        LOG.warning(GSHEET_ID_WARNING)
        return
    timer = time.time()
    result = load_gsheet_range(sheet_id, "'Rooms'!A2:F70")
    if not result:
        return
    CONFIG["zoom_rooms"] = {}
    for row in result.get("values", []):
        for _ in range(len(row), 5):
            row.append("")
        name = row[1]
        if not name:
            continue
        CONFIG["zoom_rooms"][name] = {
            "name": name,
            "enabled": row[0] != "FALSE",
            "id": row[2].strip().replace(" ", ""),
            "url": row[3] if 3 < len(row) else "",
            "utc_offset": row[4] if 4 < len(row) else "-",
            "comment": row[5] if 5 < len(row) else "",
        }
    LOG.info(
        "\U0001f4c3 Loaded %d rooms from Google Sheet in %.1f s",
        len(CONFIG["zoom_rooms"]),
        time.time() - timer,
    )


def load_gsheet_user_roles():
    """Load role symbol config data from a Google Sheet into CONFIG."""
    sheet_id = os.getenv("ZOOM_KEYHOLE_GSHEET_ID")
    if not sheet_id:
        LOG.warning(GSHEET_ID_WARNING)
        return
    timer = time.time()
    result = load_gsheet_range(sheet_id, "'Role symbols'!A2:B24")
    if not result:
        return
    for row in result.get("values", []):
        for _ in range(len(row), 2):
            row.append("")
        CONFIG["user_roles"][row[0]] = row[1]
    LOG.info(
        "\U0001f4c3 Loaded %d role symbols from Google Sheet in %.1f s",
        len(CONFIG["user_roles"]),
        time.time() - timer,
    )


def get_name_for_board_id(board_id: str) -> str:
    """Return the name associated with the board id.

    Args:
        board_id (str): Miro board ID

    Returns:
        The name of the Miro board from the server configuration
    """
    try:
        return next(
            board.get("name", "")
            for board in CONFIG["miro_boards"]
            if board["id"] == board_id
        )
    except StopIteration:
        return "NAME NOT SET"


def get_config(key: str) -> None:
    """Return the configuration for a given key."""
    with CONFIG["_mutex"]:
        return CONFIG[key]


def reset_config_section(section_name: str) -> None:
    """Reset a specific section of the CONFIG dictionary."""
    with CONFIG["_mutex"]:
        CONFIG[section_name].clear()
