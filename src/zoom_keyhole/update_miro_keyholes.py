"""Update Miro zoom room 'keyhole' widgets"""

import copy
import datetime
import html

from .config import CONFIG, HEADER_PARTICIPANT_LIST, get_name_for_board_id
from .logging import LOG
from .miro_utils import WidgetUpdateArgs, process_set_widget_text
from .update_miro_utils import get_symbol_for_user, get_symbols_for_roles


def update_zoom_room_widgets(
    room_name: str, empty_participants: str, editable: bool
):
    """Update all miro widgets for the specified room room

    Args:
        room_name (str): Zoom room name.
        empty_participants (str): String to display on empty participant list.
        editable (bool): If false, widgets will be locked to edits.
    """
    # Skip if no widgets (aka 'keyholes') are defined for this room.
    zoom_data = CONFIG.get("zoom_data", {}).get(room_name, {})
    if "keyholes" not in zoom_data:
        return

    # List of zoom room widgets (aka 'keyholes') to remove if required.
    widgets_not_found = []

    # LOG.debug("Updating zoom room widget %s", room_name)

    # Loop over keyholes.
    for i, keyhole in enumerate(zoom_data["keyholes"]):
        board_id = keyhole["board_id"]
        widget_id = keyhole["widget_id"]
        art_filter = keyhole.get("ART_filter", "")
        currently_editable = keyhole.get("editable", True)

        # Get the text to display in the widget.
        text = get_text_for_room_widget(
            room_name, empty_participants, art_filter
        )
        # LOG.debug("Get zoom room widget %s text %s", keyhole, text)

        # Only update if widget text has changed and the widget is editable
        if (
            keyhole.get("text", None) == text
            and currently_editable == editable
        ):
            continue

        keyhole["text"] = text
        keyhole["editable"] = editable

        widget_descriptor = f"zoom room {room_name}"
        args = WidgetUpdateArgs(
            board_id,
            get_name_for_board_id(board_id),
            widget_id,
            text,
            editable,
            widget_descriptor,
            widgets_not_found,
            i,
        )
        widgets_not_found = process_set_widget_text(args)

    # Forget about any deleted widgets.
    for i in sorted(widgets_not_found, reverse=True):
        zoom_data["keyholes"].pop(i)


def get_text_for_room_widget(
    room_name: str, empty_participants: str, keyhole_art: list
) -> str:
    """Construct text to display in a zoom room (a.k.a. keyhole) widget."""
    text = generate_keyhole_header(room_name, keyhole_art)

    room_config = CONFIG["zoom_rooms"].get(room_name, {})
    if not room_config.get("enabled", True):
        return add_disabled_room_message(text)

    zoom_users = get_zoom_users(room_name)
    if not zoom_users:
        return add_empty_participants_message(text, empty_participants)

    user_list = match_zoom_users_to_config(zoom_users, keyhole_art)
    user_list = add_unknown_users(user_list, zoom_users, keyhole_art)

    groups = group_users_by_team(user_list)
    return format_participant_list(text, groups)


def generate_keyhole_header(room_name: str, keyhole_art: list) -> str:
    """Generate the header text for the keyhole widget."""
    room_config = CONFIG["zoom_rooms"].get(room_name, {})
    utc_time = datetime.datetime.now(datetime.timezone.utc)
    header = (
        f"<p><strong>{room_name}</strong></p>"
        f"<p>Meeting ID: <a href='{room_config.get('url', '')}'>"
        f"{room_config.get('id', '')}</a></p>"
        f"<p>Last Updated: {utc_time.strftime('%H:%M')} UTC</p>"
    )
    if keyhole_art:
        header += f"<p>ART: {', '.join(keyhole_art)}</p>"
    if room_config.get("comment"):
        header += f"<p><i>{room_config['comment']}</i></p>"
    header += "<br/>"
    return header


def add_disabled_room_message(text: str) -> str:
    """Add a message indicating room updates are disabled."""
    list_header = CONFIG.get(
        "header_participant_list", HEADER_PARTICIPANT_LIST
    )
    return text + f"<p>{list_header}:<br/>&bull; (Updates disabled)<br/></p>"


def get_zoom_users(room_name: str) -> list:
    """Retrieve a copy of Zoom users from the configuration."""
    room_config = CONFIG["zoom_rooms"].get(room_name, {})
    with CONFIG["_mutex"]:
        return (
            copy.deepcopy(
                CONFIG["zoom_data"].get(room_name, {}).get("participants", [])
            )
            if room_config
            else []
        )


def add_empty_participants_message(text: str, empty_participants: str) -> str:
    """Add a message indicating no participants are present."""
    list_header = CONFIG.get(
        "header_participant_list", HEADER_PARTICIPANT_LIST
    )
    return text + f"<p>{list_header}:<br/>&bull; {empty_participants}<br/></p>"


def match_zoom_users_to_config(zoom_users: list, keyhole_art: list) -> list:
    """Match Zoom users to known configuration and apply ART filters."""
    user_list = []
    known_users = CONFIG.get("users", [])
    found_names = []

    for zoom_name, zoom_id in zoom_users:
        for user in known_users:
            if any(uid in zoom_name for uid in user.get("ids", "")):
                if (zoom_name, zoom_id) not in found_names:
                    found_names.append((zoom_name, zoom_id))
                if filter_user_by_art(user, keyhole_art):
                    user_list.append(user)
    remove_matched_users(zoom_users, found_names)
    return user_list


def filter_user_by_art(user: dict, keyhole_art: list) -> bool:
    """Determine if a user passes the ART filter."""
    user_art = user.get("ART", "")
    return (
        not keyhole_art
        or user_art in keyhole_art
        or (not user_art and "None" in keyhole_art)
    )


def remove_matched_users(zoom_users: list, found_names: list):
    """Remove users already matched to known configuration."""
    for zoom_name, zoom_id in found_names:
        try:
            zoom_users.remove((zoom_name, zoom_id))
        except ValueError as ex:
            LOG.warning(
                "Caught exception: %s not in list (room %s)",
                str(ex),
                zoom_name,
            )


def add_unknown_users(
    user_list: list, zoom_users: list, keyhole_art: list
) -> list:
    """Add unknown users to the list if no ART filter applies."""
    no_team_group = "(No team set)"
    for zoom_name, _ in zoom_users:
        if not keyhole_art or "None" in keyhole_art:
            user_list.append(
                {
                    "name": html.escape(zoom_name),
                    "ids": [zoom_name],
                    "roles": [""],
                    "team": [no_team_group],
                    "ART": "(No ART set)",
                    "emoji": "",
                }
            )
    return user_list


def group_users_by_team(user_list: list) -> dict:
    """Group users by their team."""
    no_team_group = "(No team set)"
    groups = {}
    for user in user_list:
        for team in user.get("team", [""]):
            if not team:
                team = no_team_group
            groups.setdefault(team, []).append(user)
    return dict(
        sorted(groups.items(), key=lambda x: (x[0] != no_team_group, x[0]))
    )


def format_participant_list(text: str, groups: dict) -> str:
    """Format the participant list for display."""
    bullet_point = (
        "&bull; "
        if CONFIG.get("user_roles", {}).get("show_bullet_point", True)
        else ""
    )
    symbol_position = (
        CONFIG.get("user_roles", {}).get("symbol_position", "left").lower()
    )

    list_header = CONFIG.get(
        "header_participant_list", HEADER_PARTICIPANT_LIST
    )
    text += f"<p>{list_header}"
    text += f" ({sum(len(users) for users in groups.values())}):<br/>"

    for team, users in groups.items():
        if len(groups) > 1:
            text += f"<strong>{team} ({len(users)})</strong><br/>"
        for user in sorted(users, key=lambda u: u["name"]):
            symbol = get_symbol_for_user(CONFIG.get("user_roles", {}), user)
            role_symbols = get_symbols_for_roles(
                CONFIG.get("user_roles", {}), user
            )
            if symbol_position == "left":
                text += f"{bullet_point}{symbol} "
                text += f"{user['name']} {role_symbols}<br/>"
            else:
                text += f"{bullet_point}{user['name']} "
                text += f"{symbol} {role_symbols}<br/>"
    text += "</p>"
    return text
