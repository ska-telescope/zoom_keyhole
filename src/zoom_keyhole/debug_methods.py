"""API calls for debugging."""

import time
import uuid
from typing import Any, Dict, Optional

from fastapi import APIRouter

from .config import (
    CONFIG,
    GSHEET_BOARDS_SCAN_PERIOD,
    GSHEET_ROLES_SCAN_PERIOD,
    GSHEET_ROOMS_SCAN_PERIOD,
    GSHEET_USERS_SCAN_PERIOD,
    MIRO_BOARD_SCAN_PERIOD,
    MIRO_BOARD_UPDATE_PERIOD,
    ZOOM_ROOM_SCAN_PERIOD,
    get_config,
    load_gsheet_miro_boards,
    load_gsheet_rooms,
    load_gsheet_users,
    reset_config_section,
)
from .miro_utils import scan_boards
from .update_miro import update_miro
from .update_miro_directories import get_text_for_directory
from .update_miro_keyholes import get_text_for_room_widget
from .zoom_utils import scan_rooms

MESSAGE_KEY = "message"
INSTANCE_ID = str(uuid.uuid4())
SERVER_STARTED_AT = time.time()

# Create the APIRouter
router = APIRouter()


def format_uptime(start_time: float) -> Dict[str, int]:
    """
    Calculate the uptime duration from a given start time.

    This function computes the uptime in terms of days, hours, and minutes,
    based on the elapsed time from the provided start time.

    Args:
        start_time (float): The epoch timestamp representing the start time.

    Returns:
        Dict[str, int]: A dictionary containing the uptime duration with keys:
            - 'days' (int): Number of full days elapsed.
            - 'hours' (int): Number of full hours elapsed, excluding days.
            - 'minutes' (int): Number of minutes elapsed, excluding days and
                               hours.
    """
    elapsed = time.time() - start_time
    days, rem = divmod(elapsed, 24 * 3600)
    hours, rem = divmod(rem, 3600)
    minutes = rem // 60
    return {"days": int(days), "hours": int(hours), "minutes": int(minutes)}


@router.get("/server/instance_id", tags=["Server"])
async def get_server_instance_id():
    """Return the instance ID, started at timestamp, and uptime.

    Returns:
        dict: The server's instance ID, started at timestamp, and
        uptime in days/hours/minutes
    """
    return {
        "instance_id": INSTANCE_ID,
        "server_started_at": time.strftime(
            "%Y-%m-%d %H:%M:%S UTC", time.gmtime(SERVER_STARTED_AT)
        ),
        "uptime": format_uptime(SERVER_STARTED_AT),
    }


@router.get("/server/update_rates", tags=["Server"])
async def get_server_update_rates():
    """Return the update rates.

    Returns:
        dict: The server's update rates
    """
    return {
        "boards_scan_period": GSHEET_BOARDS_SCAN_PERIOD,
        "roles_scan_period": GSHEET_ROLES_SCAN_PERIOD,
        "room_scan_period": GSHEET_ROOMS_SCAN_PERIOD,
        "users_scan_period": GSHEET_USERS_SCAN_PERIOD,
        "miro_board_scan_period": MIRO_BOARD_SCAN_PERIOD,
        "miro_board_update_period": MIRO_BOARD_UPDATE_PERIOD,
        "zoom_room_scan_period": ZOOM_ROOM_SCAN_PERIOD,
    }


@router.get("/config/keys", tags=["Configuration"])
async def get_config_keys():
    """Return the keys in the configuration structure.

    Returns:
        dictionary of keys in the config structure.
    """
    return {
        "keys": list(CONFIG.keys()),
        "header_participant_list": CONFIG["header_participant_list"],
        "zoom_participant_location": CONFIG["zoom_participant_location"],
        "allowed_arts": CONFIG["allowed_arts"],
        "rooms_scanned": CONFIG["rooms_scanned"],
    }


@router.patch("/config/reset", tags=["Configuration"])
async def patch_reset_config():
    """Reset the configuration."""
    reset_config_section("users")
    reset_config_section("miro_boards")
    reset_config_section("zoom_rooms")
    reset_config_section("zoom_data")
    reset_config_section("directories")
    reset_config_section("user_roles")
    return {MESSAGE_KEY: "Configuration reset"}


@router.get("/miro_boards/config", tags=["Miro Boards"])
async def get_miro_boards_config():
    """Return miro_boards configuration.

    Returns:
        dict: The server's miro_boards configuration
    """
    return {"miro_boards": get_config("miro_boards")}


@router.patch("/miro_boards/config/reset", tags=["Miro Boards"])
async def patch_miro_boards_config_reset():
    """Reset the users configuration."""
    reset_config_section("miro_boards")
    return {MESSAGE_KEY: "Miro boards Configuration reset"}


@router.patch("/miro_boards/config/reload", tags=["Miro Boards"])
async def patch_miro_boards_config_reload():
    """Reload the boards configuration."""
    load_gsheet_miro_boards()
    return {MESSAGE_KEY: "Boards configuration reloaded"}


@router.patch("/miro_boards/scan_for_widgets", tags=["Miro Boards"])
async def patch_miro_boards_scan_for_widgets():
    """Scan the Miro boards for widgets"""
    scan_boards()
    return {MESSAGE_KEY: "Miro boards scanned"}


@router.patch("/miro_boards/update", tags=["Miro Boards"])
async def patch_miro_boards_update():
    """Force update the Miro widgets."""
    update_miro()
    return {MESSAGE_KEY: "Miro widgets updated"}


@router.get("/zoom_rooms", tags=["Zoom Rooms"])
async def get_zoom_rooms_config(
    enabled_only: Optional[bool] = None,
) -> Dict[str, Any]:
    """Get Zoom rooms configuration

    Args:
        enabled_only (bool): If True, only return enabled rooms.
        Defaults to None.

    Returns:
        dict: Zoom rooms configuration.
    """
    rooms = get_config("zoom_rooms")
    if enabled_only is None:
        return {"total rooms": len(rooms), "zoom_rooms": rooms}
    filtered_rooms = {
        name: config
        for name, config in rooms.items()
        if config.get("enabled", False)
    }
    return {
        "enabled rooms": len(filtered_rooms),
        "zoom_rooms": filtered_rooms,
    }


@router.patch("/zoom_rooms/config/reset", tags=["Zoom Rooms"])
async def patch_zoom_rooms_config_reset():
    """Reset the zoom room configuration.

    Resets the configuration storing zoom room data
    (loaded from the settings)
    """
    reset_config_section("zoom_rooms")
    return {MESSAGE_KEY: "Zoom Configuration reset"}


@router.patch("/zoom_rooms/config/reload", tags=["Zoom Rooms"])
async def patch_zoom_rooms_config_reload():
    """Reload the rooms configuration."""
    load_gsheet_rooms()
    return {MESSAGE_KEY: "Rooms configuration reloaded"}


@router.get("/users/config", tags=["Users"])
async def get_users_config():
    """Return users configuration.

    Returns:
        dict: The server's users configuration
    """
    _users = get_config("users")
    return {"no. users": len(_users), "users": _users}


@router.patch("/users/config/reset", tags=["Users"])
async def patch_users_config_reset():
    """Reset the users configuration."""
    reset_config_section("users")
    return {MESSAGE_KEY: "Users Configuration reset"}


@router.patch("/users/config/reload", tags=["Users"])
async def patch_users_config_reload():
    """Reload the users configuration."""
    load_gsheet_users()
    return {MESSAGE_KEY: "Users configuration reloaded"}


@router.get("/user_roles/config", tags=["User Roles"])
async def get_user_roles_symbols_config():
    """Get the user roles config.

    Returns:
        dictionary of role symbols.
    """
    return {
        "total roles": len(CONFIG["user_roles"]),
        "role_symbols": CONFIG["user_roles"],
    }


@router.patch("/user_roles/config/reload", tags=["User Roles"])
async def patch_roles_config_reload():
    """Reload the user roles configuration."""


@router.patch("/user_roles/config/reset", tags=["User Roles"])
async def patch_user_roles_config_reset():
    """Reset the users configuration."""
    reset_config_section("user_roles")
    return {MESSAGE_KEY: "Users Roles configuration reset"}


@router.get("/directories/config", tags=["Directories"])
async def get_directories_config():
    """Return directories configuration.

    Returns:
        dict: The servers zoom keyhole directory configuration data
    """
    return {"directories": get_config("directories")}


@router.patch("/directories/config/reset", tags=["Directories"])
async def patch_directories_config_reset():
    """Reset the directories.

    Resets the zoom room directory data.
    """
    reset_config_section("directories")
    return {MESSAGE_KEY: "Directories config reset"}


@router.patch("/directories/update", tags=["Directories"])
async def patch_directories_update():
    """Scan the Miro boards."""
    scan_boards()
    return {MESSAGE_KEY: "Miro boards scanned to update directories"}


@router.get("/directories/text_for_directory", tags=["Directories"])
async def get_directory_widget_text(
    roles: str = "PM", arts: Optional[str] = None
):
    """Returns text for directory"""

    if not arts:
        arts = ""

    directory = {
        "roles_select": [i.strip() for i in roles.split(",")],
        "ART_filter": [i.strip() for i in arts.split(",")],
    }
    return {
        "Roles": roles,
        "ARTs": arts,
        "text": get_text_for_directory(directory),
    }


@router.get("/zoom_data/config", tags=["Zoom Data"])
async def get_zoom_data_config(
    only_with_keyholes: bool = True,
) -> Dict[str, Any]:
    """Return zoom data.

    Args:
        only_with_keyholes: If True, only return rooms that have keyholes
        configured

    Returns:
        Dict: The servers zoom configuration data
    """
    _zoom_data = get_config("zoom_data")
    if only_with_keyholes:
        # Filter zoom data to only include rooms with non-empty
        # keyholes
        filtered_zoom_data = {
            room: data
            for room, data in _zoom_data.items()
            if data.get("keyholes", [])
        }
        return {
            "len filtered zoom data": len(filtered_zoom_data),
            "zoom_data": filtered_zoom_data,
        }
    return {"len zoom data": len(_zoom_data), "zoom_data": _zoom_data}


@router.patch("/zoom_data/config/reset", tags=["Zoom Data"])
async def patch_zoom_data_config_reset():
    """Reset the zoom room data.

    Resets the data associated with zoom rooms. This includes
    participants, widgets etc.
    """
    reset_config_section("zoom_data")
    return {MESSAGE_KEY: "Zoom data reset"}


@router.get("/zoom_data/keyhole_text", tags=["Zoom Data"])
async def get_zoom_data_keyhole_text(
    room_name: str = "room1", art_filter: Optional[str] = None
):
    """Return keyhole text.

    Args:
        room_name: Name of the room to get text for
        art: ART filter

    Returns:
        dict: The server's keyhole text
    """
    art = []
    if art_filter:
        art = [i.strip() for i in art_filter.split(",")]
    return {
        "Room Name": room_name,
        "ART": art,
        "text": get_text_for_room_widget(
            room_name, "No participants found", art
        ),
    }


@router.patch("/zoom_data/scan_rooms", tags=["Zoom Data"])
async def patch_zoom_data_scan_rooms():
    """Manually force a scan of each zoom room for participants"""
    scan_rooms()
    return {MESSAGE_KEY: "Zoom Rooms scanned"}
