"""Update Miro."""

import time

from .config import CONFIG
from .logging import LOG
from .update_miro_directories import update_directory_widgets
from .update_miro_keyholes import update_zoom_room_widgets


def update_miro() -> None:
    """Update all widgets on all Miro boards.

    This includes
        - 'zoom room widgets' which display the participants of a room
        - 'directory widgets' which display where identified people are (
            which room)

    If successful and the update took longer than 2 seconds, a log message is
    printed.
    """
    update_timer = time.time()
    for room_name in CONFIG["zoom_data"]:
        editable = (
            not CONFIG.get("zoom_rooms", {})
            .get(room_name, {})
            .get("enabled", True)
        )
        update_zoom_room_widgets(room_name, "(Nobody)", editable)

    update_directory_widgets(editable=False)
    elapsed = time.time() - update_timer
    if elapsed > 2.0:
        LOG.info(
            "\U0001F501 Updating Miro widgets took %.1fs (only printing "
            "updates which take > 2 seconds)",
            elapsed,
        )
