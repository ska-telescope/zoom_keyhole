"""Functions to interact with Miro."""

import html
import json
import os
import re
import time
from http import HTTPStatus
from typing import NamedTuple

import requests

from .config import CONFIG
from .logging import LOG

MIRO_API_TOKENS = []
APPLICATION_JSON = "application/json"


def get_miro_api_token(api_cost: int) -> str:
    """Return the best API token to use for Miro based on usage.

    Uses the environment variable ZOOM_KEYHOLE_MIRO_API_TOKEN.

    Args:
        api_cost (int): Cost of required API call.

    Returns:
        str: API token to use. Returns an empty string if no token is found.
    """
    if len(MIRO_API_TOKENS) == 0:
        # Initialise by parsing the environment variable.
        tokens = os.getenv("ZOOM_KEYHOLE_MIRO_API_TOKEN")
        tokens = tokens.replace('"', "")
        if not tokens:
            LOG.error(
                "No Miro API token: Set the environment variable "
                "ZOOM_KEYHOLE_MIRO_API_TOKEN"
            )
            return ""
        MIRO_API_TOKENS.extend(
            [{"token": token, "usage": 0} for token in tokens.split(";")]
        )
    token_data = min(MIRO_API_TOKENS, key=lambda x: x["usage"])
    token_data["usage"] += api_cost
    return token_data["token"]


# pylint: disable=too-many-locals
def get_all_widgets_of_type(
    board_id: str,
    widget_type: str = "text",
    limit: int = 30,
    timeout: int = 15,
    cursor: str = "",
) -> list:
    """Return page of widgets up to the maximum number set by limit.

    Args:
        board_id (str): Board ID.
        widget_type (str): Type of widgets to get.
        limit (int): Maximum number of widgets to request
        timeout (int): Request timeout.
        cursor (str): point at which to start loading the new page

    Returns:
        list(dict): List of dictionaries containing widget data.
    """
    # Skip if no board ID is supplied.
    if not board_id:
        return []

    # Get the Miro API token to use.
    token = get_miro_api_token(100)  # In v2 API this is Level 2, not Level 3.
    if not token:
        return []

    # Construct the request.
    headers = {
        "Accept": APPLICATION_JSON,
        "Content-Type": APPLICATION_JSON,
        "Authorization": f"Bearer {token}",
    }
    url = f"https://api.miro.com/v2/boards/{board_id}/items/"
    # Minimum limit == 10, max limit == 50 (as of 30th Nov 2022)
    params = {"type": widget_type, "limit": limit}
    if cursor:
        params["cursor"] = cursor
    try:
        resp = requests.get(url, params, headers=headers, timeout=timeout)
    except requests.exceptions.RequestException as ex:
        LOG.error(
            "Miro API exception getting widgets (board_id=%s, limit=%d): %s",
            board_id,
            limit,
            str(ex),
        )
        return []

    if resp.status_code != HTTPStatus.OK:
        LOG.error("Error getting widgets on board %s: %s", board_id, resp.text)
        return []

    # Log Miro API usage info, if found.
    if "X-RateLimit-Limit" in resp.headers:
        reset_in = float(resp.headers["X-RateLimit-Reset"]) - time.time()
        rate_remaining = float(resp.headers["X-RateLimit-Remaining"])
        rate_limit = float(resp.headers["X-RateLimit-Limit"])
        percent = 100.0 * rate_remaining / rate_limit
        if percent < 10.0:
            LOG.warning(
                "\U0001F6A8 Miro API usage: %.0f/%.0f (reset in %.0f s)",
                rate_remaining,
                rate_limit,
                reset_in,
            )

    # Get the widget data.
    resp_data = resp.json()
    new_cursor = resp_data.get("cursor", None)
    data = resp_data.get("data", [])
    if new_cursor:
        data += get_all_widgets_of_type(
            board_id, widget_type, limit, timeout, new_cursor
        )

    # Check there are no duplicate widgets found
    check_no_duplicate_widgets(data)

    # Return the widget list.
    return data


def set_widget_text(
    board_id: str,
    widget: str,
    text: str,
    editable: bool = True,
    timeout: int = 15,
):
    """Set text of a single text widget on the board.

    Args:
        board_id (str): Board ID.
        widget (str): Widget ID.
        text (str): Text string to display.
        editable (bool): If false, widgets will be locked to edits.
        timeout (int): Request timeout.

    Returns:
        tuple (int, str): HTTP status code and response string.
    """
    # Get the Miro API token to use.
    token = get_miro_api_token(100)
    if not token:
        return (0, "")

    # Construct the request.
    headers = {
        "Accept": APPLICATION_JSON,
        "Content-Type": APPLICATION_JSON,
        "Authorization": f"Bearer {token}",
    }
    base = "https://api.miro.com/"
    endpoint = f"v1/boards/{board_id}/widgets/{widget}"
    params = json.dumps({"text": text, "capabilities": {"editable": editable}})
    # endpoint = "v2/boards/{}/texts/{}".format(board_id, widget)
    # params = json.dumps({"data": {"content": text}})
    # NOTE The v2 API does not currently have the concept of editable widgets.
    # We may want to delay adopting it at this time.
    # From https://developers.miro.com/docs/rest-api-comparison-guide#item
    #     The v1 widget object also offered capabilities and metadata.
    #     Neither of these are currently available for v2 item objects.
    try:
        resp = requests.patch(
            base + endpoint, params, headers=headers, timeout=timeout
        )
    except requests.exceptions.RequestException as ex:
        err_str = f"Miro API exception setting widget text: {str(ex)}"
        LOG.error(err_str)
        return (-1, err_str)

    # Log Miro API usage info, if found.
    if "X-RateLimit-Limit" in resp.headers:
        reset_in = float(resp.headers["X-RateLimit-Reset"]) - time.time()
        remaining = float(resp.headers["X-RateLimit-Remaining"])
        limit = float(resp.headers["X-RateLimit-Limit"])
        percent = 100.0 * remaining / limit
        if percent < 10.0:
            LOG.warning(
                "\U0001F6A8 Miro API usage: %.0f/%.0f (reset in %.0f s)",
                remaining,
                limit,
                reset_in,
            )

    return (resp.status_code, resp.text)


class WidgetUpdateArgs(NamedTuple):
    """Arguments for updating a Miro widget.

    Args:
        board_id: ID of the Miro board containing the widget
        board_name: Display name of the Miro board
        widget_id: ID of the widget to update
        text: New text content for the widget
        editable: Whether the widget should be editable by users
        widget_descriptor: Human-readable description of the widget type
        not_found: List of widgets that are not found
        index: Index of this widget in the config list
    """

    board_id: str
    board_name: str
    widget_id: str
    text: str
    editable: bool
    widget_descriptor: str
    not_found: list
    index: int


def process_set_widget_text(args: WidgetUpdateArgs):
    """Handle updating text content of a Miro widget.

    Updates the text content of a widget on a Miro board. If the update fails,
    logs an error. If the widget is not found (404), logs a warning and marks
    the widget for removal from the server configuration.

    Args:
        args (WidgetUpdateArgs): Named tuple containing all required arguments
            for updating a Miro widget, including board ID, widget ID, text
            content, and tracking info.

    Returns:
        None
    """
    # LOG.debug("Setting widget text %s text %s", args.widget_id, args.text)
    status, response_text = set_widget_text(
        args.board_id, args.widget_id, args.text, args.editable
    )
    if status != HTTPStatus.OK:
        LOG.error(
            "Error updating widget %s on board %s (%s) : %s",
            args.widget_descriptor,
            args.board_name,
            args.board_id,
            response_text,
        )
    if status == HTTPStatus.NOT_FOUND:
        LOG.warning(
            "Widget for '%s' not found on board %s (%s), "
            "removing from server data.",
            args.widget_descriptor,
            args.board_name,
            args.board_id,
        )
        args.not_found.append(args.index)
    return args.not_found


def get_widgets_on_miro_board(board_id: str, board_name: str):
    """Find handles to 'zoom keyhole' widgets on a Miro board.

    Finds Zoom room widgets and directory widgets.

    Updates CONFIG['zoom_data']

    Args:
        board_id (str): Board ID to query.
        board_name (str): Name / label of the board being updated.

    Raises:
        ValueError: if mismatch in expected number of widgets
    """
    # Precompile regex
    art_regex = re.compile(r".*ART: *([\w,;()&*{} ]+)")
    zoom_directory_regex = re.compile(
        r".*Zoom Directory:.?(?:<\/strong>)?([\w,;()&*{} ]+)"
    )
    list_header = CONFIG.get(
        "header_participant_list", "Currently in the room"
    )

    widgets = get_all_widgets_of_type(board_id)

    total_new_keyholes = 0
    total_new_directories = 0

    LOG.debug(
        "Updating widget list for Miro board '%s' (%s), %d widgets detected.",
        board_name,
        board_id,
        len(widgets),
    )

    for widget in widgets:
        text = _get_widget_text(widget, list_header)
        if not text:
            continue

        widget_data = _create_widget_data(
            board_id, widget, art_regex, text, board_name
        )

        # Process keyholes
        total_new_keyholes += _process_keyholes(
            text, board_id, board_name, widget_data
        )

        # Process directories
        total_new_directories += _process_directories(
            text, board_id, board_name, zoom_directory_regex, widget_data
        )

    _log_results(
        total_new_keyholes, total_new_directories, board_id, board_name
    )


def check_no_duplicate_widgets(widgets):
    """Validate that a list of Miro widgets contains no duplicate widget IDs.

    Args:
        widgets (list): List of dictionaries containing Miro widget data,
            where each dictionary has an 'id' key containing the widget's
            unique identifier.

    Raises:
        ValueError: If any widget IDs appear more than once in the list,
            indicating duplicate widgets were found.
    """
    widget_id_list = [w.get("id", "") for w in widgets]
    if len(set(widget_id_list)) != len(widgets):
        LOG.error(
            "Error updating Miro widgets list expecting %d widgets, got %d",
            len(widgets),
            len(set(widget_id_list)),
        )
        raise ValueError("Error updating Miro widget list!")


def _get_widget_text(widget, list_header):
    """Extract and clean widget text."""
    text = widget.get("data", {}).get("content", "")
    if not text:
        return ""
    text = html.unescape(text)
    if list_header in text:
        text = text[: text.index(list_header)]
    return text


def _create_widget_data(board_id, widget, art_regex, text, board_name):
    """Create zoom keyhole widget data dictionary with ART info."""
    widget_id = widget["id"]
    # https://miro.com/app/board/uXjVKq8iePU=/?moveToWidget=3458764598273625532&cot=10
    widget_data = {
        "board_id": board_id,
        "board_name": board_name,
        "widget_id": widget_id,
        "widget_url": f"https://miro.com/app/board/{board_id}"
        + f"/?moveToWidget={widget_id}",
        "editable": True,
        "ART_filter": [],
        "roles_select": [],
    }
    search_art = art_regex.search(text)
    allowed_arts = {
        art.lower() for art in CONFIG["allowed_arts"]
    }  # Convert allowed ARTs to lowercase
    if search_art:
        params = search_art.groups()[0].split(",")
        widget_data["ART_filter"] = [
            i.strip() for i in params if i.strip().lower() in allowed_arts
        ]
    return widget_data


def _process_keyholes(text, board_id, board_name, widget_data):
    """Process keyhole widgets.

    Updates CONFIG["zoom_data"]

    Args:
        text:
        board_id:
        board_name:
        widget_data:

    """
    total_new = 0
    text_lower = text.lower()

    for room_name in CONFIG["zoom_rooms"]:
        zoom_room_widget_data = CONFIG["zoom_data"].get(room_name)
        if not zoom_room_widget_data:
            zoom_room_widget_data = CONFIG["zoom_data"][room_name] = {}

        if "keyholes" not in zoom_room_widget_data:
            zoom_room_widget_data["keyholes"] = []

        room_name_lower = room_name.lower()

        # Add a new keyhole widget IF
        #   - widget text contains 'meeting id:' (case insensitive)
        #   - widget text contains the room name (case insensitive)
        #   - the widget is not already known about by matching the board
        #     and widget id
        if (
            "meeting id:" in text_lower
            and room_name_lower in text_lower
            and not any(
                (
                    keyhole_data["board_id"] == board_id
                    and keyhole_data["widget_id"] == widget_data["widget_id"]
                )
                for keyhole_data in zoom_room_widget_data["keyholes"]
            )
        ):
            LOG.info(
                "\U0001F510 Adding keyhole widget for room:%s on "
                "board:%s (%s, %s)",
                room_name,
                board_id,
                board_name,
                widget_data["widget_url"],
            )
            total_new += 1
            zoom_room_widget_data["keyholes"].append(widget_data)

    return total_new


def _process_directories(
    text, board_id, board_name, zoom_directory_regex, widget_data
):
    """Process directory widgets.

    Updates CONFIG["directories"]

    """
    total_new = 0
    match_directory = zoom_directory_regex.match(text)

    if match_directory and not any(
        (
            d["board_id"] == board_id
            and d["widget_id"] == widget_data["widget_id"]
        )
        for d in CONFIG["directories"]
    ):
        total_new += 1
        params = match_directory.groups()[0].split(";")

        widget_data["roles_select"] = [i.strip() for i in params[0].split(",")]
        widget_data["roles_select"] = list(
            filter(None, widget_data["roles_select"])
        )

        if len(params) > 1:
            widget_data["ART_filter"] = [
                i.strip() for i in params[1].split(",")
            ]
            widget_data["ART_filter"] = list(
                filter(None, widget_data["ART_filter"])
            )

        LOG.info(
            "\U0001F4D6 Adding directory on %s (%s): roles=%s, ARTs=%s",
            board_id,
            board_name,
            widget_data["roles_select"],
            widget_data["ART_filter"],
        )

        if not widget_data["roles_select"]:
            LOG.error(
                "\U000026D4 No roles defined for directory on board %s! "
                "Directory will not be enabled. %s",
                board_name,
                widget_data,
            )
            return 0

        CONFIG["directories"].append(widget_data)

    return total_new


def _log_results(
    total_new_keyholes, total_new_directories, board_id, board_name
):
    """Log summary of found widgets."""
    if total_new_keyholes:
        LOG.info(
            "\U0001F510 Found %d new keyhole widgets board %s (%s)",
            total_new_keyholes,
            board_id,
            board_name,
        )
    if total_new_directories:
        LOG.info(
            "\U0001F510 Found %d new directory widgets on board %s (%s)",
            total_new_directories,
            board_id,
            board_name,
        )


def scan_boards():
    """Scan each board for widgets."""
    for board in CONFIG["miro_boards"]:
        board_id = board.get("id", None)
        if not board.get("enabled", False):
            continue
        if board_id is None:
            LOG.error("Miro board ID not set!")
            continue
        board_name = board.get("name", "<Name not set>")
        LOG.info(
            "\U0001F510 Scanning widgets on board: %s (%s)",
            board_name,
            board_id,
        )
        get_widgets_on_miro_board(board_id, board_name)
