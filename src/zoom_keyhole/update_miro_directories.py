"""Update Miro directory widgets"""

import copy
import datetime

from .config import CONFIG, get_name_for_board_id
from .miro_utils import WidgetUpdateArgs, process_set_widget_text
from .update_miro_utils import get_symbol_for_role, get_symbol_for_user


def update_directory_widgets(editable: bool):
    """Update all directory widgets.

    Updates the text and editable status of all directory widgets in Miro
    boards. If a widget is not found, it will be removed from the
    configuration.

    Args:
        editable (bool): If false, widgets will be locked to edits.
    """
    widgets_not_found = []

    for index, directory in enumerate(CONFIG.get("directories", [])):
        text = get_text_for_directory(directory)

        if directory.get("text", "") == text:
            continue

        # update the directory configuration with the new text and the widget
        # editable status
        directory["text"] = text
        directory["editable"] = editable

        args = WidgetUpdateArgs(
            board_id=directory["board_id"],
            board_name=get_name_for_board_id(directory["board_id"]),
            widget_id=directory["widget_id"],
            text=text,
            editable=editable,
            widget_descriptor=_get_widget_descriptor(directory),
            not_found=widgets_not_found,
            index=index,
        )
        widgets_not_found = process_set_widget_text(args)

    # Remove deleted widgets from config
    for index in sorted(widgets_not_found, reverse=True):
        CONFIG["directories"].pop(index)


def _get_widget_descriptor(directory: dict) -> str:
    """Generate descriptive string for directory widget."""
    roles = directory.get("roles_select", [])
    arts = directory.get("ART_filter", [])
    return f"Directory: {roles}; {arts}"


def get_text_for_directory(directory: dict):
    """Construct text to display for single directory widget.

    Note the server may be updating multiple directories.

    Args:
        directory (dict): Directory configuration.

    Returns:
        str: HTML-formatted string for Zoom Keyhole Miro directory widget.
    """
    # Configuration
    user_roles = CONFIG.get("user_roles", {})
    symbol_position = user_roles.get("symbol_position", "left").lower()
    bullet_point = (
        "&bull; " if user_roles.get("show_bullet_point", True) else ""
    )

    # Header text
    text = format_directory_header(directory)

    # Deep copy of participant location map to avoid modifying shared data
    zoom_participant_location = copy_config_data("zoom_participant_location")

    # Iterate and add filtered roles
    text += format_roles_text(
        directory, zoom_participant_location, bullet_point, symbol_position
    )

    return text


def format_directory_header(directory: dict) -> str:
    """
    Format the header section of the directory text.

    Args:
        directory (dict): Directory configuration.

    Returns:
        str: HTML-formatted header string.
    """
    roles_selection = directory.get("roles_select", [])
    art_filter = directory.get("ART_filter", [])
    utc_time = datetime.datetime.now(datetime.timezone.utc)

    header = "<p><strong>Zoom Directory: "
    header += ", ".join(roles_selection)
    if art_filter:
        header += f'; {", ".join(art_filter)}'
    header += "</strong></p>"
    header += f'<p>Last Updated: {utc_time.strftime("%H:%M")} UTC</p>'

    return header


def copy_config_data(key: str):
    """Safely copy a section of the configuration.

    Args:
        key (str): The key of the configuration to copy.

    Returns:
        Any: A deep copy of the requested configuration section.
    """
    with CONFIG["_mutex"]:
        return copy.deepcopy(CONFIG.get(key, {}))


def format_roles_text(
    directory: dict,
    zoom_participant_location: dict,
    bullet_point: str,
    symbol_position: str,
) -> str:
    """
    Format text for each role and associated users.

    Args:
        directory (dict): Directory configuration.
        zoom_participant_location (dict): Participant location data.
        bullet_point (str): Bullet point symbol.
        symbol_position (str): Position of the symbol ("left" or "right").

    Returns:
        str: HTML-formatted roles section.
    """
    roles_selection = directory.get("roles_select", [])
    art_filter = directory.get("ART_filter", [])
    text = "<p>"

    for role in roles_selection:
        text += format_role_header(role)
        for user in get_users_for_role(role, art_filter):
            text += format_user_text(
                user, zoom_participant_location, bullet_point, symbol_position
            )
    text += "</p>"

    return text


def format_role_header(role: str) -> str:
    """
    Format the header for a role.

    Args:
        role (str): Role name.

    Returns:
        str: HTML-formatted role header.
    """
    role_symbol = get_symbol_for_role(CONFIG.get("user_roles", {}), role)
    return f"<br/><strong>{role} {role_symbol}</strong><br/>"


def get_users_for_role(role: str, art_filter: list) -> list:
    """
    Filter users for a specific role and ART filter.

    Args:
        role (str): Role name.
        art_filter (list): ART filter criteria.

    Returns:
        list: List of matching user dictionaries.
    """
    filtered_users = []
    for user in sorted(CONFIG.get("users", []), key=lambda u: u["name"]):
        if role not in user.get("roles", []):
            continue
        if art_filter and not any(
            art in user.get("ART", "") for art in art_filter
        ):
            continue
        filtered_users.append(user)
    return filtered_users


def format_user_text(
    user: dict,
    zoom_participant_location: dict,
    bullet_point: str,
    symbol_position: str,
) -> str:
    """
    Format text for an individual user.

    Args:
        user (dict): User data.
        zoom_participant_location (dict): Participant location data.
        bullet_point (str): Bullet point symbol.
        symbol_position (str): Position of the symbol ("left" or "right").

    Returns:
        str: HTML-formatted user entry.
    """
    name = user.get("name")
    room = get_user_room(user, zoom_participant_location)
    symbol = get_symbol_for_user(CONFIG.get("user_roles", {}), user)

    if symbol_position == "left":
        return f"{bullet_point}{symbol} {name}: {room}<br/>"
    return f"{bullet_point}{name} {symbol}: {room}<br/>"


def get_user_room(user: dict, zoom_participant_location: dict) -> str:
    """Get the room information for a user.

    Looks up a user's current Zoom room based on their IDs and returns a
    formatted string with room details including meeting ID and URL if
    available.

    Args:
        user (dict): User configuration containing name and IDs.
        zoom_participant_location (dict): Mapping of Zoom participant names
            to room locations.

    Returns:
        str: Formatted string containing room information:
            - If user is in known room with URL:
              "(<url>meeting_id, room</url>)"
            - If user is in known room without URL: "(meeting_id, room)"
            - If user not found: "(Not in a known meeting)"
    """
    partial_ids = user.get("ids", [])
    for zoom_name in zoom_participant_location:
        if any(partial_id in zoom_name for partial_id in partial_ids):
            room = zoom_participant_location[zoom_name]
            room_config = CONFIG["zoom_rooms"].get(room)
            if room_config:
                meeting_id = room_config["id"]
                meeting_url = room_config.get("url", "")
                return (
                    f'(<a href="{meeting_url}">{meeting_id}, {room}</a>)'
                    if meeting_url
                    else f"({meeting_id}, {room})"
                )
            return "(Not in a known meeting)"
    return "(Not in a known meeting)"
