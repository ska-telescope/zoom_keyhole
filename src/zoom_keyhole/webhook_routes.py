"""Webhook routes for handling Zoom events."""

import hashlib
import hmac
import os
from enum import Enum

from fastapi import APIRouter, BackgroundTasks, HTTPException, Request, status
from fastapi.responses import JSONResponse
from pydantic import ValidationError

from .config import CONFIG
from .logging import LOG
from .models.zoom import ParticipantModel

# Secret Header Token to verify webhooks
ZOOM_KEYHOLE_WEBHOOK_SECRET_TOKEN = os.getenv(
    "ZOOM_KEYHOLE_WEBHOOK_SECRET_TOKEN"
)

router = APIRouter()


class ZoomEventType(str, Enum):
    """Enumeration of Zoom webhook event types."""

    URL_VALIDATION = "endpoint.url_validation"
    PARTICIPANT_JOINED = "meeting.participant_joined"
    PARTICIPANT_LEFT = "meeting.participant_left"


async def handle_url_validation(body_json):
    """Handle Zoom URL validation event.

    see: https://developers.zoom.us/docs/api/webhooks/
    """
    plain_token = body_json["payload"]["plainToken"]
    encrypted_token = hmac.new(
        ZOOM_KEYHOLE_WEBHOOK_SECRET_TOKEN.encode("utf-8"),
        plain_token.encode("utf-8"),
        hashlib.sha256,
    ).hexdigest()
    return {
        "plainToken": plain_token,
        "encryptedToken": encrypted_token,
    }


def handle_participant_event(body, background_tasks):
    """Handle participant joined or left events."""
    try:
        participant = ParticipantModel.model_validate_json(body)
        background_tasks.add_task(
            process_meeting_participant_event, participant
        )
    except ValidationError as validation_error:
        LOG.error("Failed to validate participant model: %s", validation_error)


def _get_matching_rooms(zoom_id: str) -> list[str]:
    """Get list of room names matching the zoom ID.

    Args:
        zoom_id: Zoom meeting ID

    Returns:
        List of matching room names
    """
    return [
        room_name
        for room_name, room_config in CONFIG["zoom_rooms"].items()
        if room_config["id"] == zoom_id and room_config.get("enabled", True)
    ]


def _update_participant_state(
    room_name: str, user_name: str, user_id: str, event: str
) -> None:
    """Update participant state for a room.

    Args:
        room_name: Name of the zoom room
        user_name: Name of the participant
        user_id: ID of the participant
        event: Event type (joined/left)
    """
    room_data = CONFIG["zoom_data"].get(room_name, {})
    if "participants" not in room_data:
        room_data["participants"] = []

    user = (user_name, user_id)

    if event == ZoomEventType.PARTICIPANT_JOINED:
        CONFIG["zoom_participant_location"][user_name] = room_name
        if user not in room_data["participants"]:
            room_data["participants"].append(user)
            LOG.debug("Adding user '%s' to: %s", user, room_name)
    elif event == ZoomEventType.PARTICIPANT_LEFT:
        CONFIG["zoom_participant_location"][user_name] = ""
        if user in room_data["participants"]:
            room_data["participants"].remove(user)
            LOG.debug("Removing user '%s' from: %s", user, room_name)


async def process_meeting_participant_event(body: ParticipantModel):
    """Process incoming Zoom webhook events for meeting participant activities.

    This function handles webhook notifications from Zoom, specifically for
    participant join and leave events in meetings. It updates the internal
    state of participant presence in Zoom rooms.

    Args:
        body (ParticipantModel): The webhook payload containing event details.
            Expected to include information such as:
            - event type (join/leave)
            - meeting ID
            - participant name and ID
            - timestamp of the event

    Returns:
        dict: A response indicating the success of processing the webhook.
            Format: {"message": "ok"}

    Raises:
        ValueError: If the incoming data doesn't match the expected format.

    Note:
        This function is designed to be used asynchronously and thread-safe,
        utilizing a mutex for concurrent access to shared data structures.
    """
    # Extract event details
    event = body.event
    zoom_id = body.payload.object.id.strip().replace(" ", "")
    user_name = body.payload.object.participant.user_name
    user_id = body.payload.object.participant.user_id

    LOG.debug("Processing Event %s for user %s", event, user_name)

    # Process for each matching zoom room
    room_list = _get_matching_rooms(zoom_id)
    for room_name in room_list:
        with CONFIG["_mutex"]:
            _update_participant_state(room_name, user_name, user_id, event)

    return {"message": "ok"}


async def verify_webhook_signature(headers, request_body):
    """Verify the Zoom Webhook Payload according to Zoom Guidelines.

    Args:
        headers: HTTP header
        request_body: body of the request message

    Raises:
        HTTPException: If the webhook secret token is not found

    Returns:
        bool: True if the signature is valid, False otherwise
    """
    # https://developers.zoom.us/docs/api/rest/webhook-reference/#verify-with-zooms-header

    if ZOOM_KEYHOLE_WEBHOOK_SECRET_TOKEN is None:
        LOG.error("Failed to verify webhook, Zoom Webhook token = None!")
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Webhook Secret Token could not be found!",
        )

    try:
        message = (
            f"v0:{headers['x-zm-request-timestamp']}: "
            + f"{request_body.decode('utf-8')}"
        )
        hash_for_verify = hmac.new(
            ZOOM_KEYHOLE_WEBHOOK_SECRET_TOKEN.encode("utf-8"),
            message.encode("utf-8"),
            hashlib.sha256,
        ).hexdigest()
        signature = f"v0={hash_for_verify}"
        if headers["x-zm-signature"] == signature:
            LOG.debug("Webhook signature verified from zoom!")
            return True

        LOG.debug(
            "Webhook verification failed header=%s, signature=%s",
            headers["x-zm-signature"],
            signature,
        )
        return False
    except Exception as exc:
        LOG.error("Failed to verify webhook, %s", str(exc))
        raise HTTPException(status_code=500, detail=str(exc)) from exc


@router.post("/webhook")
async def process_webhook(request: Request, background_tasks: BackgroundTasks):
    """
    Webhook to catch Zoom events.

    Args:
        request: The webhook request.
        background_tasks: Background tasks for deferred execution.

    Raises:
        HTTPException: If the webhook signature is not valid.

    Returns:
        dict: The response to the webhook.
    """
    headers = dict(request.headers)
    body = await request.body()
    LOG.debug("Processing webhook: headers='%s' body='%s", headers, body)

    # Verify webhook signature before processing
    signature_valid = await verify_webhook_signature(headers, body)
    if not signature_valid:
        LOG.warning(
            "Invalid webhook signature. Webhook may not have come from Zoom!"
        )
        # BMo: Message turned off a a hack to proceed with webhook processing!
        # raise HTTPException(
        #     status_code=status.HTTP_401_UNAUTHORIZED,
        #     detail="Invalid webhook signature.",
        # )

    try:
        body_json = await request.json()
        event = body_json.get("event")

        # If the event is a validation event - ie zoom checking the endpoint
        # is valid
        if event == ZoomEventType.URL_VALIDATION:
            response = await handle_url_validation(body_json)
            return JSONResponse(
                content=response, status_code=status.HTTP_200_OK
            )

        # If event is a meeting participant change
        if event in (
            ZoomEventType.PARTICIPANT_JOINED,
            ZoomEventType.PARTICIPANT_LEFT,
        ):
            handle_participant_event(body, background_tasks)
            return JSONResponse(content={}, status_code=status.HTTP_200_OK)

        LOG.warning("Unsupported event type received: %s", event)
        return JSONResponse(content={}, status_code=status.HTTP_204_NO_CONTENT)
    except ValidationError as validation_error:
        LOG.error("Validation error: %s", validation_error)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Invalid request payload.",
        ) from validation_error
    except Exception as exc:
        LOG.error("Error processing webhook: %s", exc)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Internal server error.",
        ) from exc
