"""Google sheet config data classes."""
from pydantic import BaseModel


class User(BaseModel):
    """A ZoomKeyhole user."""

    name: str
    zoom_id: str = ""
    team: str = ""
    art: str = ""
    emoji: str = ""
    roles: str = ""
