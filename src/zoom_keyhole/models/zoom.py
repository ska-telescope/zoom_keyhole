"""Zoom event models."""

from typing import Optional

from pydantic import BaseModel


class ParticipantObjectModel(BaseModel):
    """."""

    user_id: str
    user_name: str
    id: str
    join_time: Optional[str] = None
    leave_time: Optional[str] = None


class MeetingObjectModel(BaseModel):
    """."""

    id: str  # meeting number
    uuid: str  # uuid for a meeting instance
    host_id: str  # id of user who is set as the host of the meeting
    topic: str  # meeting topic
    type: int  # meeting type enum (1, 2, 3, 8)
    start_time: str  # meeting start time
    timezone: str  # timezone for meeting
    duration: int  # duration for the meeting
    participant: ParticipantObjectModel  # Participant object


class EventPayloadModel(BaseModel):
    """."""

    account_id: str  # account id of the meeting host
    object: MeetingObjectModel


class ParticipantModel(BaseModel):
    """Zoom meeting.participant_left event.

    Reference: https://tinyurl.com/y5khj8a9
    """

    payload: EventPayloadModel
    event_ts: int  # timestamp
    event: str  # name of the event
