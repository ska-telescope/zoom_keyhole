"""Functions to interact with Zoom."""

import base64
import copy
import os
import signal
import threading
import time
from datetime import datetime, timezone
from http import HTTPStatus

import requests

from .config import CONFIG, ZOOM_ROOM_SCAN_PERIOD
from .logging import LOG

ZOOM_API_TOKEN = {}

CONDITION_VAR = threading.Condition()  # For thread signalling.


def get_zoom_api_token() -> str:
    """Return a Zoom API token.

    The environment variables ZOOM_KEYHOLE_ZOOM_CLIENT_ID,
    ZOOM_KEYHOLE_ZOOM_CLIENT_SECRET, and ZOOM_KEYHOLE_ZOOM_ACCOUNT_ID
    must be defined prior to calling this function.

    Returns:
        str: String containing Zoom API token.
    """
    # Check for a valid cached token.
    token = ZOOM_API_TOKEN.get("token", None)
    expiry_time = ZOOM_API_TOKEN.get("exp", None)

    # If no token, or token has expired, generate a new one.
    if not token or time.time() >= (expiry_time - 10):
        client_id = os.getenv("ZOOM_KEYHOLE_ZOOM_CLIENT_ID")
        client_secret = os.getenv("ZOOM_KEYHOLE_ZOOM_CLIENT_SECRET")
        account_id = os.getenv("ZOOM_KEYHOLE_ZOOM_ACCOUNT_ID")

        # If environment variables are not defined, return an empty string.
        if not (client_id and client_secret and account_id):
            return ""

        # Concatenate the client ID and client secret with a colon in between.
        credentials = f"{client_id}:{client_secret}"

        # Encode the credentials in Base64.
        encoded_credentials = base64.b64encode(
            credentials.encode("utf-8")
        ).decode("utf-8")

        # Request body to obtain the token.
        data = {"grant_type": "account_credentials", "account_id": account_id}

        # Request headers with the Base64-encoded credentials.
        headers = {
            "Authorization": f"Basic {encoded_credentials}",
            "Host": "zoom.us",
        }

        # Zoom OAuth endpoint.
        token_endpoint = "https://zoom.us/oauth/token"

        # POST request to get the token.
        response = requests.post(
            token_endpoint, data=data, headers=headers, timeout=30
        )

        # Check if the request was successful.
        if response.status_code == HTTPStatus.OK:
            # Parse the response to extract the access token and expiration.
            response_json = response.json()
            token = response_json["access_token"]
            valid_duration_sec = response_json["expires_in"]

            # Adding expiration time in seconds to the current time.
            expiry_time = time.time() + valid_duration_sec

            # Store the new token.
            ZOOM_API_TOKEN["token"] = token
            ZOOM_API_TOKEN["exp"] = expiry_time
        else:
            # Return an empty string if the request was unsuccessful.
            return ""

    return token


# pylint: disable=too-many-locals, too-many-return-statements
# pylint: disable=too-many-branches
def get_zoom_participants(
    meeting_id: str,
    room_name: str,
    next_page_token: str = "",
    timeout: int = 15,
    log_rate_info: bool = True,
) -> list:
    """Get list of participants in a Zoom meeting.

    Args:
        meeting_id (str): Zoom room meeting ID.
        room_name (str): Zoom room name.
        next_page_token (str): Token to use to get next page of participants.
        timeout (int): Request timeout.
        log_rate_info(bool): toggle on/off logging of API rate information.

    Returns:
        list[tuple(str, str)]: List of tuples of participant names and IDs.
    """
    # Skip if no meeting ID supplied.
    meeting_id = meeting_id.replace("-", "")
    if not meeting_id:
        return []

    # Get the Zoom API token to use.
    api_token = get_zoom_api_token()

    # Skip if no API token supplied.
    if not api_token:
        LOG.warning("No Zoom API token supplied")
        return []

    # Construct the request.
    headers = {
        "Authorization": f"Bearer {api_token}",
        "Content-Type": "application/json",
    }
    base = "https://api.zoom.us/v2"
    endpoint = f"/metrics/meetings/{meeting_id}/participants"
    params = {
        "type": "live",
        "page_size": 300,
        "next_page_token": next_page_token,
    }
    try:
        # https://developers.zoom.us/docs/api/rest/reference/zoom-api/methods/#operation/dashboardMeetingParticipants
        resp = requests.get(
            base + endpoint, params=params, headers=headers, timeout=timeout
        )
    except requests.exceptions.RequestException as ex:
        LOG.error("Zoom API request exception: %s", str(ex))
        return []

    # Exit if Zoom rate limit reached, issue SIGINT to clean up widgets.
    if resp.status_code == HTTPStatus.TOO_MANY_REQUESTS:  # code: 429
        LOG.error(
            "%s Zoom Rate limit reached, exiting! %s",
            "\U0001F631" * 3,
            "\U0001F631" * 3,
        )
        os.kill(os.getpid(), signal.SIGINT)

    if resp.status_code == HTTPStatus.NOT_FOUND:
        # LOG.debug(
        #     "Room %s (id: %s) not found, unable to poll participants",
        #     room_name,
        #     meeting_id,
        # )
        return []

    # For any other non OK response code print and error and return
    if resp.status_code != HTTPStatus.OK:
        LOG.error(
            "Zoom API request failed for room %s (%s), status: %d,"
            " resp: %s, headers: %s",
            room_name,
            meeting_id,
            resp.status_code,
            resp.text,
            resp.headers,
        )
        return []

    # Print Zoom API rate limit info
    try:
        _limit = int(resp.headers["X-RateLimit-Limit"])
        _remaining = int(resp.headers["X-RateLimit-Remaining"])
        _api_calls = _limit - _remaining
        _api_usage_percent = float(_api_calls) / float(_limit) * 100.0
        _utc_now = datetime.now(timezone.utc)
        _seconds_since_midnight_utc = (
            _utc_now
            - _utc_now.replace(hour=0, minute=0, second=0, microsecond=0)
        ).total_seconds()
        _day_fraction = (_seconds_since_midnight_utc / 86400.0) * 100.0
        if log_rate_info:
            LOG.debug(
                "\U0001F6A8 Zoom API rate limit: %d / %d day fraction: %.1f%% "
                "API usage: %.1f%% (headroom before EXIT = %.1f%%)",
                _api_calls,
                _limit,
                _day_fraction,
                _api_usage_percent,
                (_day_fraction + 2.0) - _api_usage_percent,
            )
    except KeyError:
        LOG.error(
            "Unable to retrieve Zoom rate limits: status: %d,"
            " resp: %s, json: %s, headers: %s",
            resp.status_code,
            resp.text,
            resp.json(),
            resp.headers,
        )
        return []

    # Exit if the API usage reaches specified cutoff fraction.
    # Cutoff 2% greater than day fraction to allow bursts of updates
    # to not kill the app unnecessarily.
    # Only exit if > 1h into the day to avoid time zone issues
    if (_day_fraction > (1.0 / 24.0 * 100.0)) & (
        _api_usage_percent > (_day_fraction + 2.0)
    ):
        LOG.error(
            "EXITING!! Zoom API calls reached cutoff limit "
            + "(day fraction: %.1f%%, api usage: %.1f%%)",
            _day_fraction,
            _api_usage_percent,
        )
        os.kill(os.getpid(), signal.SIGINT)

    # Pull out list of user_name values.
    # https://marketplace.zoom.us/docs/api-reference/zoom-api/dashboards/dashboardmeetingparticipants
    data = resp.json()
    current_participants = []
    for room_participants in data["participants"]:
        if "leave_time" not in room_participants:
            user = (
                room_participants["user_name"],
                room_participants["user_id"],
            )
            current_participants.append(user)

    # Recursive call if necessary to get next page of participants.
    next_page = data.get("next_page_token", "")
    if len(next_page) > 0:
        current_participants = current_participants + get_zoom_participants(
            meeting_id,
            room_name,
            next_page,
            timeout=timeout,
            log_rate_info=True,
        )
    return current_participants


def scan_rooms():
    """Scan each Zoom room for participants. Runs in a separate thread."""
    # Lock the condition variable.
    LOG.info("\U0001F50D Scanning zoom rooms ...")

    CONDITION_VAR.acquire()
    LOG.debug("\U0001F512 Acquired lock for polling rooms")

    retries = 0
    max_retries = 45

    # Loop until shut down.
    while not CONFIG.get("shutdown", False):
        # Get a local copy of the Zoom room configuration.
        zoom_rooms = copy.deepcopy(CONFIG.get("zoom_rooms", {}))

        # Make sure the configuration has been loaded.
        if len(zoom_rooms) == 0:
            if retries >= max_retries:
                LOG.error(
                    "Failed to load Zoom room config after %d"
                    " retries, exiting ...",
                    retries,
                )
                CONFIG["shutdown"] = True
                break
            LOG.info(
                "\U0001F552 Waiting for Zoom room config to load..."
                " (attempt %d/%d)",
                retries + 1,
                max_retries,
            )
            CONDITION_VAR.wait(1)
            retries += 1
            continue

        # Reset the retries counter after we have a valid config
        retries = 0
        LOG.info(
            "\U0001F4E1 Scanning for Zoom room participants in %d rooms...",
            len(zoom_rooms),
        )

        # Loop over configured rooms.
        timer = time.time()
        log_rate_info = True
        for room in zoom_rooms:
            if CONFIG.get("shutdown", False):
                break
            if not zoom_rooms[room].get("enabled", True):
                continue
            # Get current participants.
            participants = get_zoom_participants(
                zoom_rooms[room]["id"],
                room,
                log_rate_info=log_rate_info,
            )

            # Process participant list.
            with CONFIG["_mutex"]:
                # Remove all old participants from the room.
                zoom_location_map = CONFIG["zoom_participant_location"]
                for zoom_name, old_location in zoom_location_map.items():
                    if old_location == room:
                        zoom_location_map[zoom_name] = ""

                # Add new participants to the room.
                for zoom_name, _ in participants:
                    zoom_location_map[zoom_name] = room
                room_data = CONFIG["zoom_data"].get(room)
                if not room_data:
                    room_data = CONFIG["zoom_data"][room] = {}
                room_data["participants"] = participants

        # Report time taken for scan.
        LOG.info(
            "\U0001F4E1 Requesting Zoom room participants from"
            " Zoom API took %.1f s",
            time.time() - timer,
        )

        # Suspend the thread if not shutting down.
        CONFIG["rooms_scanned"] = True
        if not CONFIG.get("shutdown", False):
            CONDITION_VAR.wait(ZOOM_ROOM_SCAN_PERIOD)

    # Exit the thread if shutting down.

    CONDITION_VAR.release()
    LOG.debug("Released lock for polling rooms")
