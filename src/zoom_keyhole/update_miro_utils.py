"""Utility functions used to update Miro widgets."""

import html
import zlib

from .config import SYMBOLS
from .logging import LOG


def get_symbol_for_role(symbol_options: dict, role: str) -> str:
    """Return symbol for the given role, if any.

    Args:
        symbol_options (dict): Symbol options from global config.
        role (str): Role name.

    Returns:
        str: String containing symbol to use.
    """
    if not role:
        return ""
    symbol = ""
    try:
        symbol = symbol_options[role]
        if symbol.startswith("U"):
            symbol = "\\" + symbol
        if "\\U" in symbol:
            try:
                symbol = symbol.encode().decode("unicode-escape")
            except UnicodeDecodeError:
                LOG.error("Error getting symbol for role %s", role)
                return ""
    except KeyError:
        LOG.debug(
            "No symbol defined for role %s (symbols: %s)", role, symbol_options
        )
        return ""
    return symbol


def get_symbols_for_roles(symbol_options, user):
    """Return role symbol(s) for the given user, if any.

    Args:
        symbol_options (dict): Symbol options from global config.
        user (dict): Data for user.

    Returns:
        str: String containing symbol(s) to use.
    """
    symbol = ""
    for role in user.get("roles", []):
        if not role:
            continue
        symbol += get_symbol_for_role(symbol_options, role) + " "
    return symbol


def get_symbol_for_user(symbol_options: dict, user: dict) -> str:
    """Return a symbol for the given user.

    Args:
        symbol_options (dict): Symbol options from global config.
        user (dict): Data for user.

    Returns:
        str: String containing symbol to use.
    """
    mode = symbol_options.get("mode", "random")
    if mode == "off":
        return ""
    symbol = user.get("emoji", "")
    name = user.get("name", "")
    if symbol == "":
        if mode == "selected":
            if symbol_options.get("show_default", False):
                symbol = symbol_options.get("default", "\U00002795")
        else:
            symbol = SYMBOLS[zlib.crc32(name.encode()) % len(SYMBOLS)]
    if symbol.startswith("U"):
        symbol = "\\" + symbol
    try:
        if "\\U" in symbol:
            symbol = symbol.encode().decode("unicode-escape")
    except UnicodeDecodeError:
        LOG.error("Error getting symbol for %s", name)
        symbol = ""
    if "&#" in symbol:
        symbol = html.unescape(symbol)
    if len(symbol) > 1:
        symbol = symbol[0]
    return symbol
