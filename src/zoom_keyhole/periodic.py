"""Module with a class for defining a period asynchronous task."""

import asyncio
import traceback
from contextlib import suppress
from typing import Callable, Optional

from .logging import LOG


class Periodic:
    """Manage a periodic asynchronous task."""

    def __init__(
        self, func: Callable, interval: int, name: Optional[str] = None
    ):
        """Initialise.

        Args:
            func: Function to call as a periodic async task
            interval (int): Interval to sleep for between function calls
            name (str, optional): Name of the function, used for logging

        """
        self.func = func
        self.interval = float(interval)
        self.name = name
        self.is_started = False
        self._task = None

    async def start(self):
        """Start the task."""
        if not self.is_started:
            self.is_started = True
            if self.name:
                LOG.debug("\U00002705 Starting task: %s", self.name)
            # Start task to call func periodically.
            self._task = asyncio.ensure_future(self._run())

    async def stop(self):
        """Stop the task."""
        if self.is_started:
            self.is_started = False
            if self.name:
                LOG.debug("\U0000270B Stopping task: %s", self.name)
            # Stop task and await it stopped.
            self._task.cancel()
            with suppress(asyncio.CancelledError):
                await self._task
            if self.name:
                LOG.debug("\U00002705 Task: %s stopped", self.name)

    async def _run(self):
        """Run the task.

        Raises:
            BaseException: If the function raises an exception
        """
        while True:
            try:
                self.func()
            except BaseException as err:  # pylint: disable=broad-except
                LOG.error("Caught exception: %s", err)
                traceback.print_exc()
                raise err
            await asyncio.sleep(self.interval)
