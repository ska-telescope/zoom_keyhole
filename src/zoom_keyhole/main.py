"""Zoom Keyhole App.

This module implements the Zoom Keyhole application, which provides
functionality for displaying the list of participants in a Zoom meeting
in a set of Miro widgets. Configuration is obtained from Google sheets.
"""

import os

from fastapi import FastAPI

from .lifespan import lifespan
from .webhook_routes import router as webhook_router

ENABLE_DEBUG_METHODS = (
    os.getenv("ENABLE_DEBUG_METHODS", "true").lower() == "true"
)

APP = FastAPI(lifespan=lifespan)

APP.include_router(webhook_router)

if ENABLE_DEBUG_METHODS:
    from .debug_methods import router as debug_router

    APP.include_router(debug_router)
