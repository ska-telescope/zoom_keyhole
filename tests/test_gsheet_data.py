"""Test configuration data classes."""

from random import choice

import pytest
from assertpy import assert_that
from names import get_full_name
from pydantic import ValidationError

from zoom_keyhole.models.gsheet_data import User


def test_user_simple():
    """Test the user data class."""
    name = "user1"
    user = User(name=name)
    assert_that(user.name).is_equal_to(name)
    assert_that(user.zoom_id).is_empty()
    assert_that(user.team).is_empty()
    assert_that(user.art).is_empty()
    assert_that(user.emoji).is_empty()
    assert_that(user.roles).is_empty()


def test_user_roles():
    """Test the user data class with roles set."""
    user = User(name="user2", roles="a,b")
    assert_that(user.name).is_equal_to("user2")
    assert_that(user.roles).is_equal_to("a,b")


def test_user_roles_validation():
    """Test user roles validation.

    Test that creating a user raises a validation error if the
    roles type is not a string.

    """
    with pytest.raises(ValidationError):
        _ = User(name="user3", roles=["a", "b"])


def test_user_list():
    """Test creating and manipulating a list of users."""
    # users = []
    # names = [get_full_name() for _ in range(10)]

    # users.append(User(name="Ben Mort"))
    users = [
        User(
            name=get_full_name(),
            art=choice(["a", "b", ""]),
            roles=choice(["PM", "PO", ""]),
        )
        for _ in range(10)
    ]
    assert_that(users).is_length(10)
    print("")
    for user in users:
        print(user)

    users[7].art = "d"
    index = next((i for i, user in enumerate(users) if user.art == "d"), None)
    print(index)

    users_in_art_a = [user for user in users if user.art == "a"]
    for user in users_in_art_a:
        print(user)
