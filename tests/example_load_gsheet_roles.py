"""Test loading gsheet users."""

import logging
import sys
import time

from zoom_keyhole.config import CONFIG, load_gsheet_user_roles


# pylint: disable=duplicate-code
def init_logger():
    """Initialise the logger."""
    logger = logging.getLogger("zoom_keyhole")
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter(
        "%(levelname)-7s : %(message)-80s [%(filename)s L%(lineno)d] "
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)


def main():
    """."""
    init_logger()
    timer = time.time()
    load_gsheet_user_roles()
    print(CONFIG)
    print(time.time() - timer)


if __name__ == "__main__":
    main()
