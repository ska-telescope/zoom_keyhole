"""Utility to send mock zoom events to zoom-keyhole."""

import http.client
import json
import os
import random
import time

import names

ZOOM_KEYHOLE_HOST = os.getenv("ZOOM_KEYHOLE_HOST", "localhost")
ZOOM_KEYHOLE_PORT = int(os.getenv("ZOOM_KEYHOLE_PORT", "8000"))


def create_random_zoom_event(people, room_ids):
    """Create a random zoom event.

    Args:
        people (list): List of people to use in the event
        room_ids (list): List of room IDs to use in the event

    Returns:
        dict: A random zoom event

    """
    _events = ["meeting.participant_joined", "meeting.participant_left"]
    return {
        "event": random.choice(_events),
        "payload": {
            "account_id": "string",
            "object": {
                "id": random.choice(room_ids),
                "uuid": "string",
                "host_id": "string",
                "topic": "string",
                "type": 0,
                "start_time": "string",
                "timezone": "string",
                "duration": 0,
                "participant": {
                    "user_id": "string",
                    "user_name": random.choice(people),
                    "id": "string",
                    "join_time": "string",
                    "leave_time": "string",
                },
            },
        },
    }


def main():
    """Execute the example."""
    people = [names.get_full_name() for _ in range(10)]
    rooms = [6563092332, 94051192184, 2939114038, 5402776463]
    event_counter = 0
    while True:
        conn = http.client.HTTPConnection(ZOOM_KEYHOLE_HOST, ZOOM_KEYHOLE_PORT)
        headers = {"Content-type": "application/json"}
        _event = create_random_zoom_event(people, rooms)
        print(json.dumps(_event, indent=4))
        json_data = json.dumps(_event)
        conn.request("POST", "/webhook", json_data, headers)
        resp = conn.getresponse()
        print(resp.status, resp.reason)
        time.sleep(0.01)
        conn.close()
        event_counter += 1
        if event_counter > 100:
            break


if __name__ == "__main__":
    main()
